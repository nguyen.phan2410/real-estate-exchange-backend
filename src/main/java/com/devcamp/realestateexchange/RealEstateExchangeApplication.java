package com.devcamp.realestateexchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealEstateExchangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealEstateExchangeApplication.class, args);
	}

}

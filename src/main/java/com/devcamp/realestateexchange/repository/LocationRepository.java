package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Integer> {

}

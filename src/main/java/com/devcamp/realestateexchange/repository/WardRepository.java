package com.devcamp.realestateexchange.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer> {
    List<Ward> findByDistrictId(int districtId);
}

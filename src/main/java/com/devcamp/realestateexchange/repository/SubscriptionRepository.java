package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Subscription;

public interface SubscriptionRepository extends JpaRepository<Subscription, Integer> {

}

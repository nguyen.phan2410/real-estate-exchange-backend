package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.DesignUnit;

public interface DesignUnitRepository extends JpaRepository<DesignUnit, Integer> {

}

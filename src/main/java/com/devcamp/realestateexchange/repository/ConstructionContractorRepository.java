package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.ConstructionContractor;

public interface ConstructionContractorRepository extends JpaRepository<ConstructionContractor, Integer> {

}

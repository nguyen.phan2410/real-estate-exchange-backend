package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Role;
import com.devcamp.realestateexchange.model.ERole;

public interface RoleRepository extends JpaRepository<Role, Integer> {
  Role findByName(ERole name);
}

package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.AddressMap;

public interface AddressMapRepository extends JpaRepository<AddressMap, Integer> {

}

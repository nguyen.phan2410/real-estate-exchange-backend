package com.devcamp.realestateexchange.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Street;

public interface StreetRepository extends JpaRepository<Street, Integer> {
    List<Street> findByDistrictId(int districtId);
}

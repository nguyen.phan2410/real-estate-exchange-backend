package com.devcamp.realestateexchange.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestateexchange.entity.Realestate;
import com.devcamp.realestateexchange.model.SearchRealestate;

public interface RealestateRepository extends JpaRepository<Realestate, Integer> {

        @Query("SELECT r FROM Realestate r WHERE"
                        + " (:#{#search.provinceId} IS NULL OR provinceId = :#{#search.provinceId}) AND"
                        + " (:#{#search.districtId} IS NULL OR districtId = :#{#search.districtId}) AND"
                        + " (:#{#search.wardsId} IS NULL OR wardsId = :#{#search.wardsId}) AND"
                        + " (:#{#search.streetId} IS NULL OR streetId = :#{#search.streetId}) AND"
                        + " ((:#{#search.bedroom} IS NULL) OR (:#{#search.bedroom} = 5 AND bedroom >= 5) OR (bedroom = :#{#search.bedroom})) AND"
                        + " (:#{#search.type} IS NULL OR type = :#{#search.type}) AND"
                        + " (:#{#search.request} IS NULL OR request = :#{#search.request}) AND"
                        + " (price BETWEEN :#{#search.priceLow} AND :#{#search.priceHigh}) AND"
                        + " (approved = 1) AND"
                        + " (:#{#search.title} = '' OR title LIKE CONCAT('%', :#{#search.title}, '%'))"
                        + "ORDER BY view_num DESC")

        List<Realestate> searchRealestate(@Param("search") SearchRealestate search,
                        Pageable page);

        @Query("SELECT COUNT(r) FROM Realestate r WHERE"
                        + " (:#{#search.provinceId} IS NULL OR provinceId = :#{#search.provinceId}) AND"
                        + " (:#{#search.districtId} IS NULL OR districtId = :#{#search.districtId}) AND"
                        + " (:#{#search.wardsId} IS NULL OR wardsId = :#{#search.wardsId}) AND"
                        + " (:#{#search.streetId} IS NULL OR streetId = :#{#search.streetId}) AND"
                        + " ((:#{#search.bedroom} IS NULL) OR (:#{#search.bedroom} = 5 AND bedroom >= 5) OR (bedroom = :#{#search.bedroom})) AND"
                        + " (:#{#search.type} IS NULL OR type = :#{#search.type}) AND"
                        + " (:#{#search.request} IS NULL OR request = :#{#search.request}) AND"
                        + " (price BETWEEN :#{#search.priceLow} AND :#{#search.priceHigh}) AND"
                        + " (approved = 1) AND"
                        + " (:#{#search.title} = '' OR title LIKE CONCAT('%', :#{#search.title}, '%'))")

        Integer countRealestate(@Param("search") SearchRealestate search);

        @Query(value = "SELECT r FROM Realestate r WHERE" +
                        " (:#{#search.provinceId} IS NULL OR provinceId = :#{#search.provinceId}) AND" +
                        " (:#{#search.districtId} IS NULL OR districtId = :#{#search.districtId}) AND" +
                        " (:#{#search.customerId} IS NULL OR customerId = :#{#search.customerId}) AND" +
                        " (:#{#search.projectId} IS NULL OR projectId = :#{#search.projectId}) AND" +
                        " (:#{#search.title} = '' OR title LIKE CONCAT('%',:#{#search.title},'%'))")
        List<Realestate> findBySearch(@Param("search") Realestate search, Pageable page);

        @Query(value = "SELECT COUNT(*) FROM Realestate r WHERE" +
                        " (:#{#search.provinceId} IS NULL OR provinceId = :#{#search.provinceId}) AND" +
                        " (:#{#search.districtId} IS NULL OR districtId = :#{#search.districtId}) AND" +
                        " (:#{#search.customerId} IS NULL OR customerId = :#{#search.customerId}) AND" +
                        " (:#{#search.projectId} IS NULL OR projectId = :#{#search.projectId}) AND" +
                        " (:#{#search.title} = '' OR title LIKE CONCAT('%',:#{#search.title},'%'))")
        Integer countBySearch(@Param("search") Realestate search);

        List<Realestate> findByCustomerId(Integer customerId, Pageable page);

        Integer countByCustomerId(Integer customerId);
}

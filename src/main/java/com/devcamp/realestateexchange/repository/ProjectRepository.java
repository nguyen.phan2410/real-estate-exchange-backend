package com.devcamp.realestateexchange.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer> {
    List<Project> findByDistrictId(int districtId);

}

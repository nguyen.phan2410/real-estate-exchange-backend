package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Investor;

public interface InvestorRepository extends JpaRepository<Investor, Integer> {

}

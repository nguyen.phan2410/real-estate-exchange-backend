package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.MasterLayout;

public interface MasterLayoutRepository extends JpaRepository<MasterLayout, Integer> {

}

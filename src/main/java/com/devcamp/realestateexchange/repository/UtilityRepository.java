package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Utility;

public interface UtilityRepository extends JpaRepository<Utility, Integer> {

}

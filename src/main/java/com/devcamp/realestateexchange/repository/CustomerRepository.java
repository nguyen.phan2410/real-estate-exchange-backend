package com.devcamp.realestateexchange.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.realestateexchange.entity.Customer;
import com.devcamp.realestateexchange.entity.Realestate;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    Customer findByUsername(String username);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    boolean existsByMobile(String mobile);

    @Query(value = "SELECT c FROM Customer c WHERE" +
            " (:#{#search.email} = '' OR email LIKE CONCAT('%',:#{#search.email},'%')) AND" +
            " (:#{#search.mobile} = '' OR mobile LIKE CONCAT('%',:#{#search.mobile},'%')) AND" +
            " (:#{#search.contactName} = '' OR contactName LIKE CONCAT('%',:#{#search.contactName},'%'))")
    List<Customer> findBySearch(@Param("search") Customer search, Pageable page);

    @Query(value = "SELECT COUNT(*) FROM Customer c WHERE" +
            " (:#{#search.email} = '' OR email LIKE CONCAT('%',:#{#search.email},'%')) AND" +
            " (:#{#search.mobile} = '' OR mobile LIKE CONCAT('%',:#{#search.mobile},'%')) AND" +
            " (:#{#search.contactName} = '' OR contactName LIKE CONCAT('%',:#{#search.contactName},'%'))")
    Integer countBySearch(@Param("search") Customer search);
}

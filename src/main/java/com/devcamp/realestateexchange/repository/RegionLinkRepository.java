package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.RegionLink;

public interface RegionLinkRepository extends JpaRepository<RegionLink, Integer> {

}

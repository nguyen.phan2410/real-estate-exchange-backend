package com.devcamp.realestateexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchange.entity.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {

}

package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.AddressMap;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.AddressMapRepository;

@Service
public class AddressMapService {
    @Autowired
    AddressMapRepository addressMapRepository;

    // service to get all AddressMap
    // input: page, size
    // output: list of AddressMap
    public List<AddressMap> getAllAddressMaps(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<AddressMap> addressMaps = new ArrayList<>();
        addressMapRepository.findAll(pageable).forEach(addressMaps::add);
        return addressMaps;
    }

    // service to get all AddressMap By Databale
    // input: request
    // output: list of AddressMap
    public DatatablePage<AddressMap> getAllAddressMapsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<AddressMap> addressMaps = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            addressMapRepository.findAll(pageable).forEach(addressMaps::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            addressMapRepository.findAll(pageable).forEach(addressMaps::add);
        }

        long total = addressMapRepository.count();
        DatatablePage<AddressMap> data = new DatatablePage<AddressMap>();
        data.setData(addressMaps);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get AddressMap by id
    // input: addressMapId
    // output: a AddressMap if found, null otherwise
    public AddressMap getAddressMapById(int addressMapId) {
        Optional<AddressMap> addressMap = addressMapRepository.findById(addressMapId);
        if (addressMap.isPresent()) {
            return addressMap.get();
        } else {
            return null;
        }
    }

    // service to create a AddressMap
    // input: a AddressMap obj
    // ouput: created AddressMap obj
    public AddressMap createAddressMap(AddressMap addressMap) {
        return addressMapRepository.save(addressMap);
    }

    // service to update a AddressMap
    // input: a AddressMap obj, addressMapId
    // ouput: updated AddressMap obj, null if not found
    public AddressMap updateAddressMap(int addressMapId, AddressMap addressMap) {
        Optional<AddressMap> updatedAddressMap = addressMapRepository.findById(addressMapId);
        if (updatedAddressMap.isPresent()) {
            AddressMap _addressMap = updatedAddressMap.get();
            _addressMap.setAddress(addressMap.getAddress());
            _addressMap.setLattitude(addressMap.getLattitude());
            _addressMap.setLongtitude(addressMap.getLongtitude());
            AddressMap savedAddressMap = addressMapRepository.save(_addressMap);
            return savedAddressMap;
        } else {
            return null;
        }
    }

    // service to delete AddressMap by id
    // input: addressMapId
    // output: a AddressMap if found, null otherwise
    public AddressMap deleteAddressMapById(int addressMapId) {
        Optional<AddressMap> addressMap = addressMapRepository.findById(addressMapId);
        if (addressMap.isPresent()) {
            addressMapRepository.deleteById(addressMapId);
            return addressMap.get();
        } else {
            return null;
        }
    }
}

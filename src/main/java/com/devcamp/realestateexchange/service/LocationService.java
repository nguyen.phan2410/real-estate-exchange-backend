package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.Location;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.LocationRepository;

@Service
public class LocationService {
    @Autowired
    LocationRepository locationRepository;

    // service to get all Location
    // input: page, size
    // output: list of Location
    public List<Location> getAllLocations(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Location> locations = new ArrayList<>();
        locationRepository.findAll(pageable).forEach(locations::add);
        return locations;
    }

    // service to get all Location By Databale
    // input: request
    // output: list of Location
    public DatatablePage<Location> getAllLocationsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Location> locations = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            locationRepository.findAll(pageable).forEach(locations::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            locationRepository.findAll(pageable).forEach(locations::add);
        }

        long total = locationRepository.count();
        DatatablePage<Location> data = new DatatablePage<Location>();
        data.setData(locations);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Location by id
    // input: locationId
    // output: a Location if found, null otherwise
    public Location getLocationById(int locationId) {
        Optional<Location> location = locationRepository.findById(locationId);
        if (location.isPresent()) {
            return location.get();
        } else {
            return null;
        }
    }

    // service to create a Location
    // input: a Location obj
    // ouput: created Location obj
    public Location createLocation(Location location) {
        return locationRepository.save(location);
    }

    // service to update a Location
    // input: a Location obj, locationId
    // ouput: updated Location obj, null if not found
    public Location updateLocation(int locationId, Location location) {
        Optional<Location> updatedLocation = locationRepository.findById(locationId);
        if (updatedLocation.isPresent()) {
            Location _location = updatedLocation.get();
            _location.setLatitude(location.getLatitude());
            _location.setLongitude(location.getLongitude());
            Location savedLocation = locationRepository.save(_location);
            return savedLocation;
        } else {
            return null;
        }
    }

    // service to delete Location by id
    // input: locationId
    // output: a Location if found, null otherwise
    public Location deleteLocationById(int locationId) {
        Optional<Location> location = locationRepository.findById(locationId);
        if (location.isPresent()) {
            locationRepository.deleteById(locationId);
            return location.get();
        } else {
            return null;
        }
    }
}

package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.Subscription;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.SubscriptionRepository;

@Service
public class SubscriptionService {
    @Autowired
    SubscriptionRepository subscriptionRepository;

    // service to get all Subscription
    // input: page, size
    // output: list of Subscription
    public List<Subscription> getAllSubscriptions(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Subscription> subscriptions = new ArrayList<>();
        subscriptionRepository.findAll(pageable).forEach(subscriptions::add);
        return subscriptions;
    }

    // service to get all Subscription By Databale
    // input: request
    // output: list of Subscription
    public DatatablePage<Subscription> getAllSubscriptionsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Subscription> subscriptions = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            subscriptionRepository.findAll(pageable).forEach(subscriptions::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            subscriptionRepository.findAll(pageable).forEach(subscriptions::add);
        }

        long total = subscriptionRepository.count();
        DatatablePage<Subscription> data = new DatatablePage<Subscription>();
        data.setData(subscriptions);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Subscription by id
    // input: subscriptionId
    // output: a Subscription if found, null otherwise
    public Subscription getSubscriptionById(int subscriptionId) {
        Optional<Subscription> subscription = subscriptionRepository.findById(subscriptionId);
        if (subscription.isPresent()) {
            return subscription.get();
        } else {
            return null;
        }
    }

    // service to create a Subscription
    // input: a Subscription obj
    // ouput: created Subscription obj
    public Subscription createSubscription(Subscription subscription) {
        return subscriptionRepository.save(subscription);
    }

    // service to update a Subscription
    // input: a Subscription obj, subscriptionId
    // ouput: updated Subscription obj, null if not found
    public Subscription updateSubscription(int subscriptionId, Subscription subscription) {
        Optional<Subscription> updatedSubscription = subscriptionRepository.findById(subscriptionId);
        if (updatedSubscription.isPresent()) {
            Subscription _subscription = updatedSubscription.get();
            _subscription.setUser(subscription.getUser());
            _subscription.setEndpoint(subscription.getEndpoint());
            _subscription.setPublickey(subscription.getPublickey());
            _subscription.setAuthenticationtoken(subscription.getAuthenticationtoken());
            _subscription.setContentencoding(subscription.getContentencoding());

            Subscription savedSubscription = subscriptionRepository.save(_subscription);
            return savedSubscription;
        } else {
            return null;
        }
    }

    // service to delete Subscription by id
    // input: subscriptionId
    // output: a Subscription if found, null otherwise
    public Subscription deleteSubscriptionById(int subscriptionId) {
        Optional<Subscription> subscription = subscriptionRepository.findById(subscriptionId);
        if (subscription.isPresent()) {
            subscriptionRepository.deleteById(subscriptionId);
            return subscription.get();
        } else {
            return null;
        }
    }
}

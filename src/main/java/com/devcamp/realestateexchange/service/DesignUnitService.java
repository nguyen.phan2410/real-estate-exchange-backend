package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.DesignUnit;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.DesignUnitRepository;

@Service
public class DesignUnitService {
    @Autowired
    DesignUnitRepository designUnitRepository;

    // service to get all DesignUnit
    // input: page, size
    // output: list of DesignUnit
    public List<DesignUnit> getAllDesignUnits(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<DesignUnit> designUnits = new ArrayList<>();
        designUnitRepository.findAll(pageable).forEach(designUnits::add);
        return designUnits;
    }

    // service to get all DesignUnit By Databale
    // input: request
    // output: list of DesignUnit
    public DatatablePage<DesignUnit> getAllDesignUnitsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<DesignUnit> designUnits = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            designUnitRepository.findAll(pageable).forEach(designUnits::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            designUnitRepository.findAll(pageable).forEach(designUnits::add);
        }

        long total = designUnitRepository.count();
        DatatablePage<DesignUnit> data = new DatatablePage<DesignUnit>();
        data.setData(designUnits);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get DesignUnit by id
    // input: designUnitId
    // output: a DesignUnit if found, null otherwise
    public DesignUnit getDesignUnitById(int designUnitId) {
        Optional<DesignUnit> designUnit = designUnitRepository.findById(designUnitId);
        if (designUnit.isPresent()) {
            return designUnit.get();
        } else {
            return null;
        }
    }

    // service to create a DesignUnit
    // input: a DesignUnit obj
    // ouput: created DesignUnit obj
    public DesignUnit createDesignUnit(DesignUnit designUnit) {
        return designUnitRepository.save(designUnit);
    }

    // service to update a DesignUnit
    // input: a DesignUnit obj, designUnitId
    // ouput: updated DesignUnit obj, null if not found
    public DesignUnit updateDesignUnit(int designUnitId, DesignUnit designUnit) {
        Optional<DesignUnit> updatedDesignUnit = designUnitRepository.findById(designUnitId);
        if (updatedDesignUnit.isPresent()) {
            DesignUnit _designUnit = updatedDesignUnit.get();
            _designUnit.setName(designUnit.getName());
            _designUnit.setDescription(designUnit.getDescription());
            _designUnit.setProjects(designUnit.getProjects());
            _designUnit.setAddress(designUnit.getAddress());
            _designUnit.setPhone(designUnit.getPhone());
            _designUnit.setPhone2(designUnit.getPhone2());
            _designUnit.setFax(designUnit.getFax());
            _designUnit.setEmail(designUnit.getEmail());
            _designUnit.setWebsite(designUnit.getWebsite());
            _designUnit.setNote(designUnit.getNote());
            DesignUnit savedDesignUnit = designUnitRepository.save(_designUnit);
            return savedDesignUnit;
        } else {
            return null;
        }
    }

    // service to delete DesignUnit by id
    // input: designUnitId
    // output: a DesignUnit if found, null otherwise
    public DesignUnit deleteDesignUnitById(int designUnitId) {
        Optional<DesignUnit> designUnit = designUnitRepository.findById(designUnitId);
        if (designUnit.isPresent()) {
            designUnitRepository.deleteById(designUnitId);
            return designUnit.get();
        } else {
            return null;
        }
    }
}

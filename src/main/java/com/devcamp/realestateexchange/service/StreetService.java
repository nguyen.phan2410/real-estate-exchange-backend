package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.District;
import com.devcamp.realestateexchange.entity.Street;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.DistrictRepository;
import com.devcamp.realestateexchange.repository.StreetRepository;

@Service
public class StreetService {
    @Autowired
    StreetRepository streetRepository;

    @Autowired
    DistrictRepository districtRepository;

    // service to get all Street
    // input: page, size
    // output: list of Street
    public List<Street> getAllStreets(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Street> streets = new ArrayList<>();
        streetRepository.findAll(pageable).forEach(streets::add);
        return streets;
    }

    // service to get all Street By Databale
    // input: request
    // output: list of Street
    public DatatablePage<Street> getAllStreetsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Street> streets = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            streetRepository.findAll(pageable).forEach(streets::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            streetRepository.findAll(pageable).forEach(streets::add);
        }

        long total = streetRepository.count();
        DatatablePage<Street> data = new DatatablePage<Street>();
        data.setData(streets);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Street by id
    // input: streetId
    // output: a Street if found, null otherwise
    public Street getStreetById(int streetId) {
        Optional<Street> street = streetRepository.findById(streetId);
        if (street.isPresent()) {
            return street.get();
        } else {
            return null;
        }
    }

    // service to get Street by District id
    // input: districtId
    // output: lisf of Street if District found, null otherwise
    public List<Street> getStreetsByDistrictId(int districtId) {
        Optional<District> district = districtRepository.findById(districtId);
        if (district.isPresent()) {
            return streetRepository.findByDistrictId(districtId);
        } else {
            return null;
        }
    }

    // service to create a Street
    // input: a Street obj
    // ouput: created Street obj
    public Street createStreet(Street street) {
        return streetRepository.save(street);
    }

    // service to update a Street
    // input: a Street obj, streetId
    // ouput: updated Street obj, null if not found
    public Street updateStreet(int streetId, Street street) {
        Optional<Street> updatedStreet = streetRepository.findById(streetId);
        if (updatedStreet.isPresent()) {
            Street _street = updatedStreet.get();
            _street.setName(street.getName());
            _street.setPrefix(street.getPrefix());
            _street.setProvinceId(street.getProvinceId());
            _street.setDistrictId(street.getDistrictId());

            Street savedStreet = streetRepository.save(_street);
            return savedStreet;
        } else {
            return null;
        }
    }

    // service to delete Street by id
    // input: streetId
    // output: a Street if found, null otherwise
    public Street deleteStreetById(int streetId) {
        Optional<Street> street = streetRepository.findById(streetId);
        if (street.isPresent()) {
            streetRepository.deleteById(streetId);
            return street.get();
        } else {
            return null;
        }
    }
}

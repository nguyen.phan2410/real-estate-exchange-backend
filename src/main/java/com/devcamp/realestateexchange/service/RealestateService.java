package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.Realestate;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.model.SearchRealestate;
import com.devcamp.realestateexchange.repository.RealestateRepository;

@Service
public class RealestateService {
    @Autowired
    RealestateRepository realestateRepository;

    // service to get all Realestate
    // input: page, size
    // output: list of Realestate
    public List<Realestate> getAllRealestates(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Realestate> realestates = new ArrayList<>();
        realestateRepository.findAll(pageable).forEach(realestates::add);
        return realestates;
    }

    // service to get all Realestate By Customer By Databale
    // input: request
    // output: list of Realestate
    public DatatablePage<Realestate> getAllRealestatesDataTableBySearch(HttpServletRequest request,
            Realestate realestate) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        String sortColName = request.getParameter("columns[" +
                request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Realestate> realestates = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size,
                    Sort.by(sortColName).descending());
            realestateRepository.findBySearch(realestate,
                    pageable).forEach(realestates::add);
        } else {
            Pageable pageable = PageRequest.of(page, size,
                    Sort.by(sortColName).ascending());
            realestateRepository.findBySearch(realestate,
                    pageable).forEach(realestates::add);
        }

        long total = realestateRepository.countBySearch(realestate);
        DatatablePage<Realestate> data = new DatatablePage<Realestate>();
        data.setData(realestates);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Realestate by search
    // input: SearchRealestate
    // output: List of Realestate
    public List<Realestate> getRealesateBySearch(SearchRealestate search, int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        return realestateRepository.searchRealestate(search, pageable);
    }

    // service to get Realestate by search
    // input: SearchRealestate
    // output: List of Realestate
    public Integer countRealesateBySearch(SearchRealestate search) {
        return realestateRepository.countRealestate(search);
    }

    // service to get all Realestate By Customer
    // input: request
    // output: list of Realestate
    public List<Realestate> getAllRealestatesByCustomer(
            Integer customerId) {
        // get the data
        Pageable pageable = PageRequest.of(0, Integer.MAX_VALUE);
        return realestateRepository.findByCustomerId(customerId, pageable);
    }

    // service to get Realestate by id
    // input: realestateId
    // output: a Realestate if found, null otherwise
    public Realestate getRealestateById(int realestateId) {
        Optional<Realestate> realestate = realestateRepository.findById(realestateId);
        if (realestate.isPresent()) {
            return realestate.get();
        } else {
            return null;
        }
    }

    // service to create a Realestate
    // input: a Realestate obj
    // ouput: created Realestate obj
    public Realestate createRealestate(Realestate realestate) {
        return realestateRepository.save(realestate);
    }

    // service to update a Realestate
    // input: a Realestate obj, realestateId
    // ouput: updated Realestate obj, null if not found
    public Realestate updateRealestate(int realestateId, Realestate realestate) {
        Optional<Realestate> updatedRealestate = realestateRepository.findById(realestateId);
        if (updatedRealestate.isPresent()) {
            Realestate _realestate = updatedRealestate.get();
            _realestate.setTitle(realestate.getTitle());
            _realestate.setType(realestate.getType());
            _realestate.setRequest(realestate.getRequest());
            _realestate.setProvinceId(realestate.getProvinceId());
            _realestate.setDistrictId(realestate.getDistrictId());
            _realestate.setWardsId(realestate.getWardsId());
            _realestate.setStreetId(realestate.getStreetId());
            _realestate.setProjectId(realestate.getProjectId());
            _realestate.setAddress(realestate.getAddress());
            _realestate.setCustomerId(realestate.getCustomerId());
            _realestate.setPrice(realestate.getPrice());
            _realestate.setPriceMin(realestate.getPriceMin());
            _realestate.setPriceTime(realestate.getPriceTime());
            _realestate.setAcreage(realestate.getAcreage());
            _realestate.setDirection(realestate.getDirection());
            _realestate.setTotalFloors(realestate.getTotalFloors());
            _realestate.setNumberFloors(realestate.getNumberFloors());
            _realestate.setBath(realestate.getBath());
            _realestate.setApartCode(realestate.getApartCode());
            _realestate.setWallArea(realestate.getWallArea());
            _realestate.setBedroom(realestate.getBedroom());
            _realestate.setBalcony(realestate.getBalcony());
            _realestate.setLandscapeView(realestate.getLandscapeView());
            _realestate.setApartLoca(realestate.getApartLoca());
            _realestate.setApartType(realestate.getApartType());
            _realestate.setFurnitureType(realestate.getFurnitureType());
            _realestate.setPriceRent(realestate.getPriceRent());
            _realestate.setReturnRate(realestate.getReturnRate());
            _realestate.setLegalDoc(realestate.getLegalDoc());
            _realestate.setDescription(realestate.getDescription());
            _realestate.setWidthY(realestate.getWidthY());
            _realestate.setLongX(realestate.getLongX());
            _realestate.setStreetHouse(realestate.getStreetHouse());
            _realestate.setFsbo(realestate.getFsbo());
            _realestate.setViewNum(realestate.getViewNum());
            _realestate.setCreateBy(realestate.getCreateBy());
            _realestate.setUpdateBy(realestate.getUpdateBy());
            _realestate.setShape(realestate.getShape());
            _realestate.setDistance2Facade(realestate.getDistance2Facade());
            _realestate.setAdjacentFacadeNum(realestate.getAdjacentFacadeNum());
            _realestate.setAdjacentRoad(realestate.getAdjacentRoad());
            _realestate.setAlleyMinWidth(realestate.getAlleyMinWidth());
            _realestate.setAdjacentAlleyMinWidth(realestate.getAdjacentAlleyMinWidth());
            _realestate.setFactor(realestate.getFactor());
            _realestate.setStructure(realestate.getStructure());
            _realestate.setDtsxd(realestate.getDtsxd());
            _realestate.setClcl(realestate.getClcl());
            _realestate.setCtxdPrice(realestate.getCtxdPrice());
            _realestate.setCtxdValue(realestate.getCtxdValue());
            _realestate.setPhoto(realestate.getPhoto());
            _realestate.setLatitude(realestate.getLatitude());
            _realestate.setLongtitude(realestate.getLongtitude());
            _realestate.setApproved(realestate.getApproved());

            Realestate savedRealestate = realestateRepository.save(_realestate);
            return savedRealestate;
        } else {
            return null;
        }
    }

    // service to delete Realestate by id
    // input: realestateId
    // output: a Realestate if found, null otherwise
    public Realestate deleteRealestateById(int realestateId) {
        Optional<Realestate> realestate = realestateRepository.findById(realestateId);
        if (realestate.isPresent()) {
            realestateRepository.deleteById(realestateId);
            return realestate.get();
        } else {
            return null;
        }
    }
}

package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.District;
import com.devcamp.realestateexchange.entity.Project;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.DistrictRepository;
import com.devcamp.realestateexchange.repository.ProjectRepository;

@Service
public class ProjectService {
    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    DistrictRepository districtRepository;

    // service to get all Project
    // input: page, size
    // output: list of Project
    public List<Project> getAllProjects(int page, int size) {
        if (page == 0 && size == 0) {
            size = (int) projectRepository.count();
        }

        Pageable pageable = PageRequest.of(page, size);

        List<Project> projects = new ArrayList<>();
        projectRepository.findAll(pageable).forEach(projects::add);
        return projects;
    }

    // service to get all Project By Databale
    // input: request
    // output: list of Project
    public DatatablePage<Project> getAllProjectsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Project> projects = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            projectRepository.findAll(pageable).forEach(projects::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            projectRepository.findAll(pageable).forEach(projects::add);
        }

        long total = projectRepository.count();
        DatatablePage<Project> data = new DatatablePage<Project>();
        data.setData(projects);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Project by id
    // input: projectId
    // output: a Project if found, null otherwise
    public Project getProjectById(int projectId) {
        Optional<Project> project = projectRepository.findById(projectId);
        if (project.isPresent()) {
            return project.get();
        } else {
            return null;
        }
    }

    // service to get Project by District id
    // input: districtId
    // output: lisf of Project if District found, null otherwise
    public List<Project> getProjectsByDistrictId(int districtId) {
        Optional<District> district = districtRepository.findById(districtId);
        if (district.isPresent()) {
            return projectRepository.findByDistrictId(districtId);
        } else {
            return null;
        }
    }

    // service to create a Project
    // input: a Project obj
    // ouput: created Project obj
    public Project createProject(Project project) {
        return projectRepository.save(project);
    }

    // service to update a Project
    // input: a Project obj, projectId
    // ouput: updated Project obj, null if not found
    public Project updateProject(int projectId, Project project) {
        Optional<Project> updatedProject = projectRepository.findById(projectId);
        if (updatedProject.isPresent()) {
            Project _project = updatedProject.get();
            _project.setName(project.getName());
            _project.setProvinceId(project.getProvinceId());
            _project.setDistrictId(project.getDistrictId());
            _project.setWardId(project.getWardId());
            _project.setStreetId(project.getStreetId());
            _project.setAddress(project.getAddress());
            _project.setSlogan(project.getSlogan());
            _project.setDescription(project.getDescription());
            _project.setAcreage(project.getAcreage());
            _project.setConstructArea(project.getConstructArea());
            _project.setNumBlock(project.getNumBlock());
            _project.setNumFloors(project.getNumFloors());
            _project.setNumApartment(project.getNumApartment());
            _project.setApartmentArea(project.getApartmentArea());
            _project.setInvestor(project.getInvestor());
            _project.setConstructionContractor(project.getConstructionContractor());
            _project.setDesignUnit(project.getDesignUnit());
            _project.setUtilities(project.getUtilities());
            _project.setRegionLink(project.getRegionLink());
            _project.setPhoto(project.getPhoto());
            _project.setLatitude(project.getLatitude());
            _project.setLongtitude(project.getLongtitude());

            Project savedProject = projectRepository.save(_project);
            return savedProject;
        } else {
            return null;
        }
    }

    // service to delete Project by id
    // input: projectId
    // output: a Project if found, null otherwise
    public Project deleteProjectById(int projectId) {
        Optional<Project> project = projectRepository.findById(projectId);
        if (project.isPresent()) {
            projectRepository.deleteById(projectId);
            return project.get();
        } else {
            return null;
        }
    }
}

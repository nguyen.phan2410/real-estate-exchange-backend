package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.Province;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.ProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    ProvinceRepository provinceRepository;

    // service to get all Province
    // input: page, size
    // output: list of Province
    public List<Province> getAllProvinces(int page, int size) {
        if (page == 0 && size == 0) {
            size = (int) provinceRepository.count();
        }
        Pageable pageable = PageRequest.of(page, size);
        List<Province> provinces = new ArrayList<>();
        provinceRepository.findAll(pageable).forEach(provinces::add);
        return provinces;
    }

    // service to get all Province By Databale
    // input: request
    // output: list of Province
    public DatatablePage<Province> getAllProvincesDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Province> provinces = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            provinceRepository.findAll(pageable).forEach(provinces::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            provinceRepository.findAll(pageable).forEach(provinces::add);
        }

        long total = provinceRepository.count();
        DatatablePage<Province> data = new DatatablePage<Province>();
        data.setData(provinces);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Province by id
    // input: provinceId
    // output: a Province if found, null otherwise
    public Province getProvinceById(int provinceId) {
        Optional<Province> province = provinceRepository.findById(provinceId);
        if (province.isPresent()) {
            return province.get();
        } else {
            return null;
        }
    }

    // service to create a Province
    // input: a Province obj
    // ouput: created Province obj
    public Province createProvince(Province province) {
        return provinceRepository.save(province);
    }

    // service to update a Province
    // input: a Province obj, provinceId
    // ouput: updated Province obj, null if not found
    public Province updateProvince(int provinceId, Province province) {
        Optional<Province> updatedProvince = provinceRepository.findById(provinceId);
        if (updatedProvince.isPresent()) {
            Province _province = updatedProvince.get();
            _province.setName(province.getName());
            _province.setCode(province.getCode());
            Province savedProvince = provinceRepository.save(_province);
            return savedProvince;
        } else {
            return null;
        }
    }

    // service to delete Province by id
    // input: provinceId
    // output: a Province if found, null otherwise
    public Province deleteProvinceById(int provinceId) {
        Optional<Province> province = provinceRepository.findById(provinceId);
        if (province.isPresent()) {
            provinceRepository.deleteById(provinceId);
            return province.get();
        } else {
            return null;
        }
    }
}

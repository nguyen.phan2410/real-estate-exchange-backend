package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.District;
import com.devcamp.realestateexchange.entity.Province;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.DistrictRepository;
import com.devcamp.realestateexchange.repository.ProvinceRepository;

@Service
public class DistrictService {
    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    ProvinceRepository provinceRepository;

    // service to get all District
    // input: page, size
    // output: list of District
    public List<District> getAllDistricts(int page, int size) {
        if (page == 0 && size == 0) {
            size = (int) districtRepository.count();
        }
        Pageable pageable = PageRequest.of(page, size);
        List<District> districts = new ArrayList<>();
        districtRepository.findAll(pageable).forEach(districts::add);
        return districts;
    }

    // service to get all District By Databale
    // input: request
    // output: list of District
    public DatatablePage<District> getAllDistrictsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<District> districts = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            districtRepository.findAll(pageable).forEach(districts::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            districtRepository.findAll(pageable).forEach(districts::add);
        }

        long total = districtRepository.count();
        DatatablePage<District> data = new DatatablePage<District>();
        data.setData(districts);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get District by id
    // input: districtId
    // output: a District if found, null otherwise
    public District getDistrictById(int districtId) {
        Optional<District> district = districtRepository.findById(districtId);
        if (district.isPresent()) {
            return district.get();
        } else {
            return null;
        }
    }

    // service to get District by Provice id
    // input: provinceId
    // output: lisf of District if Province found, null otherwise
    public List<District> getDistrictsByProvinceId(int provinceId) {
        Optional<Province> province = provinceRepository.findById(provinceId);
        if (province.isPresent()) {
            return districtRepository.findByProvinceId(provinceId);
        } else {
            return null;
        }
    }

    // service to create a District
    // input: a District obj
    // ouput: created District obj
    public District createDistrict(District district) {
        return districtRepository.save(district);
    }

    // service to update a District
    // input: a District obj, districtId
    // ouput: updated District obj, null if not found
    public District updateDistrict(int districtId, District district) {
        Optional<District> updatedDistrict = districtRepository.findById(districtId);
        if (updatedDistrict.isPresent()) {
            District _district = updatedDistrict.get();
            _district.setName(district.getName());
            _district.setPrefix(district.getPrefix());
            _district.setProvinceId(district.getProvinceId());
            District savedDistrict = districtRepository.save(_district);
            return savedDistrict;
        } else {
            return null;
        }
    }

    // service to delete District by id
    // input: districtId
    // output: a District if found, null otherwise
    public District deleteDistrictById(int districtId) {
        Optional<District> district = districtRepository.findById(districtId);
        if (district.isPresent()) {
            districtRepository.deleteById(districtId);
            return district.get();
        } else {
            return null;
        }
    }
}

package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.District;
import com.devcamp.realestateexchange.entity.Province;
import com.devcamp.realestateexchange.entity.Ward;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.DistrictRepository;
import com.devcamp.realestateexchange.repository.WardRepository;

@Service
public class WardService {
    @Autowired
    WardRepository wardRepository;

    @Autowired
    DistrictRepository districtRepository;

    // service to get all Ward
    // input: page, size
    // output: list of Ward
    public List<Ward> getAllWards(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Ward> wards = new ArrayList<>();
        wardRepository.findAll(pageable).forEach(wards::add);
        return wards;
    }

    // service to get all Ward By Databale
    // input: request
    // output: list of Ward
    public DatatablePage<Ward> getAllWardsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Ward> wards = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            wardRepository.findAll(pageable).forEach(wards::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            wardRepository.findAll(pageable).forEach(wards::add);
        }

        long total = wardRepository.count();
        DatatablePage<Ward> data = new DatatablePage<Ward>();
        data.setData(wards);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Ward by id
    // input: wardId
    // output: a Ward if found, null otherwise
    public Ward getWardById(int wardId) {
        Optional<Ward> ward = wardRepository.findById(wardId);
        if (ward.isPresent()) {
            return ward.get();
        } else {
            return null;
        }
    }

    // service to get Ward by District id
    // input: districtId
    // output: lisf of Ward if District found, null otherwise
    public List<Ward> getWardsByDistrictId(int districtId) {
        Optional<District> district = districtRepository.findById(districtId);
        if (district.isPresent()) {
            return wardRepository.findByDistrictId(districtId);
        } else {
            return null;
        }
    }

    // service to create a Ward
    // input: a Ward obj
    // ouput: created Ward obj
    public Ward createWard(Ward ward) {
        return wardRepository.save(ward);
    }

    // service to update a Ward
    // input: a Ward obj, wardId
    // ouput: updated Ward obj, null if not found
    public Ward updateWard(int wardId, Ward ward) {
        Optional<Ward> updatedWard = wardRepository.findById(wardId);
        if (updatedWard.isPresent()) {
            Ward _ward = updatedWard.get();
            _ward.setName(ward.getName());
            _ward.setPrefix(ward.getPrefix());
            _ward.setProvinceId(ward.getProvinceId());
            _ward.setDistrictId(ward.getDistrictId());

            Ward savedWard = wardRepository.save(_ward);
            return savedWard;
        } else {
            return null;
        }
    }

    // service to delete Ward by id
    // input: wardId
    // output: a Ward if found, null otherwise
    public Ward deleteWardById(int wardId) {
        Optional<Ward> ward = wardRepository.findById(wardId);
        if (ward.isPresent()) {
            wardRepository.deleteById(wardId);
            return ward.get();
        } else {
            return null;
        }
    }
}

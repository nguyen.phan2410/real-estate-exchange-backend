package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.Employee;
import com.devcamp.realestateexchange.entity.Role;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.model.ERole;
import com.devcamp.realestateexchange.repository.EmployeeRepository;
import com.devcamp.realestateexchange.repository.RoleRepository;
import com.devcamp.realestateexchange.security.jwt.JwtUtils;
import com.devcamp.realestateexchange.security.services.UserDetailsImpl;

@Service
public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    // service to get all Employee
    // input: page, size
    // output: list of Employee
    public List<Employee> getAllEmployees(int page, int size) {
        if (page == 0 && size == 0) {
            size = (int) employeeRepository.count();
        }
        Pageable pageable = PageRequest.of(page, size);
        List<Employee> employees = new ArrayList<>();
        employeeRepository.findAll(pageable).forEach(employees::add);
        return employees;
    }

    // service to get all Employee By Databale
    // input: request
    // output: list of Employee
    public DatatablePage<Employee> getAllEmployeesDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Employee> employees = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            employeeRepository.findAll(pageable).forEach(employees::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            employeeRepository.findAll(pageable).forEach(employees::add);
            ;
        }

        long total = employeeRepository.count();
        DatatablePage<Employee> data = new DatatablePage<>();
        data.setData(employees);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Employee by id
    // input: employeeId
    // output: a Employee if found, null otherwise
    public Employee getEmployeeById(int employeeId) {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if (employee.isPresent()) {
            return employee.get();
        } else {
            return null;
        }
    }

    // service to create a Employee
    // input: a Employee obj
    // ouput: created Employee obj
    public Employee createEmployee(Employee employee) {
        if (employeeRepository.existsByUsername(employee.getUsername())) {
            throw new RuntimeException("Username is already in use");
        }

        if (employeeRepository.existsByEmail(employee.getEmail())) {
            throw new RuntimeException("Email is already in use");
        }

        employee.setPassword(encoder.encode(employee.getPassword()));

        if (employee.getUserLevelId() == null) {
            employee.setUserLevel(roleRepository.findByName(ERole.ROLE_DEFAULT));
        } else {
            Optional<Role> role = roleRepository.findById(employee.getUserLevelId());
            if (role.isPresent()) {
                employee.setUserLevel(role.get());
            } else {
                throw new RuntimeException("No role found");
            }
        }
        return employeeRepository.save(employee);
    }

    // service to update a Employee
    // input: a Employee obj, employeeId
    // ouput: updated Employee obj, null if not found
    public Employee updateEmployee(int employeeId, Employee employee) {
        Optional<Employee> updatedEmployee = employeeRepository.findById(employeeId);
        if (updatedEmployee.isPresent()) {
            Employee _employee = updatedEmployee.get();
            if (!employee.getUsername().equals(_employee.getUsername())
                    && employeeRepository.existsByUsername(employee.getUsername())) {
                throw new RuntimeException("Username is already in use");
            }

            if (!employee.getEmail().equals(_employee.getEmail())
                    && employeeRepository.existsByEmail(employee.getEmail())) {
                throw new RuntimeException("Email is already in use");
            }

            if (!employee.getPassword().equals("")) {
                _employee.setPassword(encoder.encode(employee.getPassword()));
            }

            // username get change, update the token with new username
            if (!_employee.getUsername().equals(employee.getUsername())) {
                // Get the current authentication from the security context
                Authentication currentAuth = SecurityContextHolder.getContext().getAuthentication();
                UserDetailsImpl userDetails = (UserDetailsImpl) currentAuth.getPrincipal();
                userDetails.setUsername("Employee_" + employee.getUsername());
                // Create a new authentication object with the updated username
                Authentication newAuth = new UsernamePasswordAuthenticationToken(
                        userDetails, currentAuth.getCredentials(), currentAuth.getAuthorities());

                // Set the new authentication in the security context
                SecurityContextHolder.getContext().setAuthentication(newAuth);
                // Generate a new JWT token with the updated authentication
                String jwt = jwtUtils.generateJwtToken(newAuth);
                _employee.setToken(jwt);
            }
            _employee.setLastName(employee.getLastName());
            _employee.setFirstName(employee.getFirstName());
            _employee.setTitle(employee.getTitle());
            _employee.setTitleOfCourtesy(employee.getTitleOfCourtesy());
            _employee.setBirthDate(employee.getBirthDate());
            _employee.setHireDate(employee.getHireDate());
            _employee.setAddress(employee.getAddress());
            _employee.setCity(employee.getCity());
            _employee.setRegion(employee.getRegion());
            _employee.setPostalCode(employee.getPostalCode());
            _employee.setCountry(employee.getCountry());
            _employee.setHomePhone(employee.getHomePhone());
            _employee.setExtension(employee.getExtension());
            _employee.setPhoto(employee.getPhoto());
            _employee.setNotes(employee.getNotes());
            _employee.setReportsTo(employee.getReportsTo());
            _employee.setUsername(employee.getUsername());
            if (!employee.getPassword().equals("")) {
                _employee.setPassword(encoder.encode(employee.getPassword()));
            }
            _employee.setEmail(employee.getEmail());
            _employee.setActivated(employee.getActivated());
            _employee.setProfile(employee.getProfile());
            Optional<Role> role = roleRepository.findById(employee.getUserLevelId());
            if (role.isPresent()) {
                _employee.setUserLevel(role.get());
            } else {
                throw new RuntimeException("No role found");
            }

            Employee savedEmployee = employeeRepository.save(_employee);
            return savedEmployee;
        } else {
            return null;
        }
    }

    // service to delete Employee by id
    // input: employeeId
    // output: a Employee if found, null otherwise
    public Employee deleteEmployeeById(int employeeId) {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if (employee.isPresent()) {
            employeeRepository.deleteById(employeeId);
            return employee.get();
        } else {
            return null;
        }
    }
}

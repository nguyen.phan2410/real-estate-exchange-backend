package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.Customer;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.model.ERole;
import com.devcamp.realestateexchange.repository.CustomerRepository;
import com.devcamp.realestateexchange.repository.RoleRepository;
import com.devcamp.realestateexchange.security.jwt.JwtUtils;
import com.devcamp.realestateexchange.security.services.UserDetailsImpl;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    JwtUtils jwtUtils;

    // service to get all Customer
    // input: page, size
    // output: list of Customer
    public List<Customer> getAllCustomers(int page, int size) {
        if (page == 0 && size == 0) {
            size = (int) customerRepository.count();
        }
        Pageable pageable = PageRequest.of(page, size);
        List<Customer> customers = new ArrayList<>();
        customerRepository.findAll(pageable).forEach(customers::add);
        return customers;
    }

    // service to get all Customer By Databale
    // input: request
    // output: list of Customer
    public DatatablePage<Customer> getAllCustomersDataTableBySearch(HttpServletRequest request, Customer customer) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Customer> customers = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            customerRepository.findBySearch(customer, pageable).forEach(customers::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            customerRepository.findBySearch(customer, pageable).forEach(customers::add);
        }

        long total = customerRepository.countBySearch(customer);
        DatatablePage<Customer> data = new DatatablePage<Customer>();
        data.setData(customers);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Customer by id
    // input: customerId
    // output: a Customer if found, null otherwise
    public Customer getCustomerById(int customerId) {
        Optional<Customer> customer = customerRepository.findById(customerId);
        if (customer.isPresent()) {
            return customer.get();
        } else {
            return null;
        }
    }

    // service to create a Customer
    // input: a Customer obj
    // ouput: created Customer obj
    public Customer createCustomer(Customer customer) {
        if (customerRepository.existsByUsername(customer.getUsername())) {
            throw new RuntimeException("Username is already in use");
        }

        if (customerRepository.existsByEmail(customer.getEmail())) {
            throw new RuntimeException("Email is already in use");
        }
        customer.setPassword(encoder.encode(customer.getPassword()));
        customer.setUserLevel(roleRepository.findByName(ERole.ROLE_DEFAULT));
        return customerRepository.save(customer);
    }

    // service to update a Customer
    // input: a Customer obj, customerId
    // ouput: updated Customer obj, null if not found
    public Customer updateCustomer(int customerId, Customer customer) {
        Optional<Customer> updatedCustomer = customerRepository.findById(customerId);
        if (updatedCustomer.isPresent()) {
            Customer _customer = updatedCustomer.get();
            if (!customer.getUsername().equals(_customer.getUsername())
                    && customerRepository.existsByUsername(customer.getUsername())) {
                throw new RuntimeException("Username is already in use");
            }

            if (!customer.getEmail().equals(_customer.getEmail())
                    && customerRepository.existsByEmail(customer.getEmail())) {
                throw new RuntimeException("Email is already in use");
            }

            if (!customer.getMobile().equals(_customer.getMobile())
                    && customerRepository.existsByMobile(customer.getMobile())) {
                throw new RuntimeException("Mobile is already in use");
            }
            if (!customer.getPassword().equals("")) {
                _customer.setPassword(encoder.encode(customer.getPassword()));
            }

            // username get change, update the token with new username
            if (!_customer.getUsername().equals(customer.getUsername())) {
                // Get the current authentication from the security context
                Authentication currentAuth = SecurityContextHolder.getContext().getAuthentication();
                UserDetailsImpl userDetails = (UserDetailsImpl) currentAuth.getPrincipal();
                userDetails.setUsername("Customer_" + customer.getUsername());
                // Create a new authentication object with the updated username
                Authentication newAuth = new UsernamePasswordAuthenticationToken(
                        userDetails, currentAuth.getCredentials(), currentAuth.getAuthorities());

                // Set the new authentication in the security context
                SecurityContextHolder.getContext().setAuthentication(newAuth);
                // Generate a new JWT token with the updated authentication
                String jwt = jwtUtils.generateJwtToken(newAuth);
                _customer.setToken(jwt);
            }

            _customer.setUsername(customer.getUsername());
            _customer.setEmail(customer.getEmail());
            _customer.setMobile(customer.getMobile());
            _customer.setContactName(customer.getContactName());
            _customer.setContactTitle(customer.getContactTitle());
            _customer.setAddress(customer.getAddress());
            _customer.setMobile(customer.getMobile());
            _customer.setEmail(customer.getEmail());
            _customer.setNote(customer.getNote());
            _customer.setCreateBy(customer.getCreateBy());
            _customer.setUpdateBy(customer.getUpdateBy());
            _customer.setCreateDate(customer.getCreateDate());
            _customer.setUpdateDate(customer.getUpdateDate());

            Customer savedCustomer = customerRepository.save(_customer);
            return savedCustomer;
        } else {
            return null;
        }
    }

    // service to delete Customer by id
    // input: customerId
    // output: a Customer if found, null otherwise
    public Customer deleteCustomerById(int customerId) {
        Optional<Customer> customer = customerRepository.findById(customerId);
        if (customer.isPresent()) {
            customerRepository.deleteById(customerId);
            return customer.get();
        } else {
            return null;
        }
    }
}

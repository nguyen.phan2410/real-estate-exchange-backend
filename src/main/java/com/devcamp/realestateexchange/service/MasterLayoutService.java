package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.MasterLayout;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.MasterLayoutRepository;

@Service
public class MasterLayoutService {
    @Autowired
    MasterLayoutRepository masterLayoutRepository;

    // service to get all MasterLayout
    // input: page, size
    // output: list of MasterLayout
    public List<MasterLayout> getAllMasterLayouts(int page, int size) {
        if (page == 0 && size == 0) {
            size = (int) masterLayoutRepository.count();
        }
        Pageable pageable = PageRequest.of(page, size);
        List<MasterLayout> masterLayouts = new ArrayList<>();
        masterLayoutRepository.findAll(pageable).forEach(masterLayouts::add);
        return masterLayouts;
    }

    // service to get all MasterLayout By Databale
    // input: request
    // output: list of MasterLayout
    public DatatablePage<MasterLayout> getAllMasterLayoutsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;

        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<MasterLayout> masterLayouts = new ArrayList<>();
        System.out.println("page:" + page);
        System.out.println(size);
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            masterLayoutRepository.findAll(pageable).forEach(masterLayouts::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            masterLayoutRepository.findAll(pageable).forEach(masterLayouts::add);
        }

        long total = masterLayoutRepository.count();
        DatatablePage<MasterLayout> data = new DatatablePage<MasterLayout>();
        data.setData(masterLayouts);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get MasterLayout by id
    // input: masterLayoutId
    // output: a MasterLayout if found, null otherwise
    public MasterLayout getMasterLayoutById(int masterLayoutId) {
        Optional<MasterLayout> masterLayout = masterLayoutRepository.findById(masterLayoutId);
        if (masterLayout.isPresent()) {
            return masterLayout.get();
        } else {
            return null;
        }
    }

    // service to create a MasterLayout
    // input: a MasterLayout obj
    // ouput: created MasterLayout obj
    public MasterLayout createMasterLayout(MasterLayout masterLayout) {
        return masterLayoutRepository.save(masterLayout);
    }

    // service to update a MasterLayout
    // input: a MasterLayout obj, masterLayoutId
    // ouput: updated MasterLayout obj, null if not found
    public MasterLayout updateMasterLayout(int masterLayoutId, MasterLayout masterLayout) {
        Optional<MasterLayout> updatedMasterLayout = masterLayoutRepository.findById(masterLayoutId);
        if (updatedMasterLayout.isPresent()) {
            MasterLayout _masterLayout = updatedMasterLayout.get();
            _masterLayout.setName(masterLayout.getName());
            _masterLayout.setDescription(masterLayout.getDescription());
            _masterLayout.setProjectId(masterLayout.getProjectId());
            _masterLayout.setAcreage(masterLayout.getAcreage());
            _masterLayout.setApartmentList(masterLayout.getApartmentList());
            _masterLayout.setPhoto(masterLayout.getPhoto());

            MasterLayout savedMasterLayout = masterLayoutRepository.save(_masterLayout);
            return savedMasterLayout;
        } else {
            return null;
        }
    }

    // service to delete MasterLayout by id
    // input: masterLayoutId
    // output: a MasterLayout if found, null otherwise
    public MasterLayout deleteMasterLayoutById(int masterLayoutId) {
        Optional<MasterLayout> masterLayout = masterLayoutRepository.findById(masterLayoutId);
        if (masterLayout.isPresent()) {
            masterLayoutRepository.deleteById(masterLayoutId);
            return masterLayout.get();
        } else {
            return null;
        }
    }
}

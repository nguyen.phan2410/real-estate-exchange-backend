package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.Utility;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.UtilityRepository;

@Service
public class UtilityService {
    @Autowired
    UtilityRepository utilityRepository;

    // service to get all Utility
    // input: page, size
    // output: list of Utility
    public List<Utility> getAllUtilitys(int page, int size) {
        if (page == 0 && size == 0) {
            size = (int) utilityRepository.count();
        }
        Pageable pageable = PageRequest.of(page, size);
        List<Utility> utilitys = new ArrayList<>();
        utilityRepository.findAll(pageable).forEach(utilitys::add);
        return utilitys;
    }

    // service to get all Utility By Databale
    // input: request
    // output: list of Utility
    public DatatablePage<Utility> getAllUtilitysDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Utility> utilitys = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            utilityRepository.findAll(pageable).forEach(utilitys::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            utilityRepository.findAll(pageable).forEach(utilitys::add);
        }

        long total = utilityRepository.count();
        DatatablePage<Utility> data = new DatatablePage<Utility>();
        data.setData(utilitys);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Utility by id
    // input: utilityId
    // output: a Utility if found, null otherwise
    public Utility getUtilityById(int utilityId) {
        Optional<Utility> utility = utilityRepository.findById(utilityId);
        if (utility.isPresent()) {
            return utility.get();
        } else {
            return null;
        }
    }

    // service to create a Utility
    // input: a Utility obj
    // ouput: created Utility obj
    public Utility createUtility(Utility utility) {
        return utilityRepository.save(utility);
    }

    // service to update a Utility
    // input: a Utility obj, utilityId
    // ouput: updated Utility obj, null if not found
    public Utility updateUtility(int utilityId, Utility utility) {
        Optional<Utility> updatedUtility = utilityRepository.findById(utilityId);
        if (updatedUtility.isPresent()) {
            Utility _utility = updatedUtility.get();
            _utility.setName(utility.getName());
            _utility.setDescription(utility.getDescription());
            _utility.setPhoto(utility.getPhoto());

            Utility savedUtility = utilityRepository.save(_utility);
            return savedUtility;
        } else {
            return null;
        }
    }

    // service to delete Utility by id
    // input: utilityId
    // output: a Utility if found, null otherwise
    public Utility deleteUtilityById(int utilityId) {
        Optional<Utility> utility = utilityRepository.findById(utilityId);
        if (utility.isPresent()) {
            utilityRepository.deleteById(utilityId);
            return utility.get();
        } else {
            return null;
        }
    }
}

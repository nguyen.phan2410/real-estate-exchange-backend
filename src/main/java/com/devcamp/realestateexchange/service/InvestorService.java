package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.Investor;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.InvestorRepository;

@Service
public class InvestorService {
    @Autowired
    InvestorRepository investorRepository;

    // service to get all Investor
    // input: page, size
    // output: list of Investor
    public List<Investor> getAllInvestors(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<Investor> investors = new ArrayList<>();
        investorRepository.findAll(pageable).forEach(investors::add);
        return investors;
    }

    // service to get all Investor By Databale
    // input: request
    // output: list of Investor
    public DatatablePage<Investor> getAllInvestorsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Investor> investors = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            investorRepository.findAll(pageable).forEach(investors::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            investorRepository.findAll(pageable).forEach(investors::add);
        }

        long total = investorRepository.count();
        DatatablePage<Investor> data = new DatatablePage<Investor>();
        data.setData(investors);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get Investor by id
    // input: investorId
    // output: a Investor if found, null otherwise
    public Investor getInvestorById(int investorId) {
        Optional<Investor> investor = investorRepository.findById(investorId);
        if (investor.isPresent()) {
            return investor.get();
        } else {
            return null;
        }
    }

    // service to create a Investor
    // input: a Investor obj
    // ouput: created Investor obj
    public Investor createInvestor(Investor investor) {
        return investorRepository.save(investor);
    }

    // service to update a Investor
    // input: a Investor obj, investorId
    // ouput: updated Investor obj, null if not found
    public Investor updateInvestor(int investorId, Investor investor) {
        Optional<Investor> updatedInvestor = investorRepository.findById(investorId);
        if (updatedInvestor.isPresent()) {
            Investor _investor = updatedInvestor.get();
            _investor.setName(investor.getName());
            _investor.setDescription(investor.getDescription());
            _investor.setProjects(investor.getProjects());
            _investor.setAddress(investor.getAddress());
            _investor.setPhone(investor.getPhone());
            _investor.setPhone(investor.getPhone2());
            _investor.setFax(investor.getFax());
            _investor.setEmail(investor.getEmail());
            _investor.setWebsite(investor.getWebsite());
            _investor.setNote(investor.getNote());

            Investor savedInvestor = investorRepository.save(_investor);
            return savedInvestor;
        } else {
            return null;
        }
    }

    // service to delete Investor by id
    // input: investorId
    // output: a Investor if found, null otherwise
    public Investor deleteInvestorById(int investorId) {
        Optional<Investor> investor = investorRepository.findById(investorId);
        if (investor.isPresent()) {
            investorRepository.deleteById(investorId);
            return investor.get();
        } else {
            return null;
        }
    }
}

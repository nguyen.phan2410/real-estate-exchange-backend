package com.devcamp.realestateexchange.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageService {

    public String saveFile(MultipartFile file) {
        try {
            if (!file.isEmpty()) {
                String fileName = Instant.now().getEpochSecond() + "_" + file.getOriginalFilename();
                Path root = Paths.get("uploads");
                if (!Files.exists(root)) {
                    Files.createDirectories(root);
                }
                Path filePath = root.resolve(fileName);
                file.transferTo(filePath);
                return fileName;
            }

            return null;
        } catch (IOException e) {
            throw new RuntimeException("Could not save file");
        }

    }

    public void deleteFile(String fileName) {
        try {
            Path root = Paths.get("uploads");
            Path filePath = root.resolve(fileName);
            if (Files.exists(filePath) && root != filePath) {
                Files.delete(filePath);
            }

        } catch (IOException e) {
            throw new RuntimeException("Could not delete file");
        }

    }
}

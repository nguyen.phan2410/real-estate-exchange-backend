package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.AddressMap;
import com.devcamp.realestateexchange.entity.ConstructionContractor;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.ConstructionContractorRepository;

@Service
public class ConstructionContractorService {
    @Autowired
    ConstructionContractorRepository constructionContractorRepository;

    // service to get all ConstructionContractor
    // input: page, size
    // output: list of ConstructionContractor
    public List<ConstructionContractor> getAllConstructionContractors(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        List<ConstructionContractor> constructionContractors = new ArrayList<>();
        constructionContractorRepository.findAll(pageable).forEach(constructionContractors::add);
        return constructionContractors;
    }

    // service to get all ConstructionContractor By Databale
    // input: request
    // output: list of ConstructionContractor
    public DatatablePage<ConstructionContractor> getAllConstructionContractorsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<ConstructionContractor> constructionContractors = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            constructionContractorRepository.findAll(pageable).forEach(constructionContractors::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            constructionContractorRepository.findAll(pageable).forEach(constructionContractors::add);
        }

        long total = constructionContractorRepository.count();
        DatatablePage<ConstructionContractor> data = new DatatablePage<ConstructionContractor>();
        data.setData(constructionContractors);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get ConstructionContractor by id
    // input: constructionContractorId
    // output: a ConstructionContractor if found, null otherwise
    public ConstructionContractor getConstructionContractorById(int constructionContractorId) {
        Optional<ConstructionContractor> constructionContractor = constructionContractorRepository
                .findById(constructionContractorId);
        if (constructionContractor.isPresent()) {
            return constructionContractor.get();
        } else {
            return null;
        }
    }

    // service to create a ConstructionContractor
    // input: a ConstructionContractor obj
    // ouput: created ConstructionContractor obj
    public ConstructionContractor createConstructionContractor(ConstructionContractor constructionContractor) {
        return constructionContractorRepository.save(constructionContractor);
    }

    // service to update a ConstructionContractor
    // input: a ConstructionContractor obj, constructionContractorId
    // ouput: updated ConstructionContractor obj, null if not found
    public ConstructionContractor updateConstructionContractor(int constructionContractorId,
            ConstructionContractor constructionContractor) {
        Optional<ConstructionContractor> updatedConstructionContractor = constructionContractorRepository
                .findById(constructionContractorId);
        if (updatedConstructionContractor.isPresent()) {
            ConstructionContractor _constructionContractor = updatedConstructionContractor.get();
            _constructionContractor.setName(constructionContractor.getName());
            _constructionContractor.setDescription(constructionContractor.getDescription());
            _constructionContractor.setProjects(constructionContractor.getProjects());
            _constructionContractor.setAddress(constructionContractor.getAddress());
            _constructionContractor.setPhone(constructionContractor.getPhone());
            _constructionContractor.setPhone2(constructionContractor.getPhone2());
            _constructionContractor.setFax(constructionContractor.getFax());
            _constructionContractor.setEmail(constructionContractor.getEmail());
            _constructionContractor.setWebsite(constructionContractor.getWebsite());
            _constructionContractor.setNote(constructionContractor.getNote());
            ConstructionContractor savedConstructionContractor = constructionContractorRepository
                    .save(_constructionContractor);
            return savedConstructionContractor;
        } else {
            return null;
        }
    }

    // service to delete ConstructionContractor by id
    // input: constructionContractorId
    // output: a ConstructionContractor if found, null otherwise
    public ConstructionContractor deleteConstructionContractorById(int constructionContractorId) {
        Optional<ConstructionContractor> constructionContractor = constructionContractorRepository
                .findById(constructionContractorId);
        if (constructionContractor.isPresent()) {
            constructionContractorRepository.deleteById(constructionContractorId);
            return constructionContractor.get();
        } else {
            return null;
        }
    }
}

package com.devcamp.realestateexchange.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchange.entity.RegionLink;
import com.devcamp.realestateexchange.model.DatatablePage;
import com.devcamp.realestateexchange.repository.RegionLinkRepository;

@Service
public class RegionLinkService {
    @Autowired
    RegionLinkRepository regionLinkRepository;

    // service to get all RegionLink
    // input: page, size
    // output: list of RegionLink
    public List<RegionLink> getAllRegionLinks(int page, int size) {
        if (page == 0 && size == 0) {
            size = (int) regionLinkRepository.count();
        }
        Pageable pageable = PageRequest.of(page, size);
        List<RegionLink> regionLinks = new ArrayList<>();
        regionLinkRepository.findAll(pageable).forEach(regionLinks::add);
        return regionLinks;
    }

    // service to get all RegionLink By Databale
    // input: request
    // output: list of RegionLink
    public DatatablePage<RegionLink> getAllRegionLinksDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<RegionLink> regionLinks = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            regionLinkRepository.findAll(pageable).forEach(regionLinks::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            regionLinkRepository.findAll(pageable).forEach(regionLinks::add);
        }

        long total = regionLinkRepository.count();
        DatatablePage<RegionLink> data = new DatatablePage<RegionLink>();
        data.setData(regionLinks);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    // service to get RegionLink by id
    // input: regionLinkId
    // output: a RegionLink if found, null otherwise
    public RegionLink getRegionLinkById(int regionLinkId) {
        Optional<RegionLink> regionLink = regionLinkRepository.findById(regionLinkId);
        if (regionLink.isPresent()) {
            return regionLink.get();
        } else {
            return null;
        }
    }

    // service to create a RegionLink
    // input: a RegionLink obj
    // ouput: created RegionLink obj
    public RegionLink createRegionLink(RegionLink regionLink) {
        return regionLinkRepository.save(regionLink);
    }

    // service to update a RegionLink
    // input: a RegionLink obj, regionLinkId
    // ouput: updated RegionLink obj, null if not found
    public RegionLink updateRegionLink(int regionLinkId, RegionLink regionLink) {
        Optional<RegionLink> updatedRegionLink = regionLinkRepository.findById(regionLinkId);
        if (updatedRegionLink.isPresent()) {
            RegionLink _regionLink = updatedRegionLink.get();
            _regionLink.setName(regionLink.getName());
            _regionLink.setDescription(regionLink.getDescription());
            _regionLink.setPhoto(regionLink.getPhoto());
            _regionLink.setAddress(regionLink.getAddress());
            _regionLink.setLattitude(regionLink.getLattitude());
            _regionLink.setLongtitude(regionLink.getLongtitude());

            RegionLink savedRegionLink = regionLinkRepository.save(_regionLink);
            return savedRegionLink;
        } else {
            return null;
        }
    }

    // service to delete RegionLink by id
    // input: regionLinkId
    // output: a RegionLink if found, null otherwise
    public RegionLink deleteRegionLinkById(int regionLinkId) {
        Optional<RegionLink> regionLink = regionLinkRepository.findById(regionLinkId);
        if (regionLink.isPresent()) {
            regionLinkRepository.deleteById(regionLinkId);
            return regionLink.get();
        } else {
            return null;
        }
    }
}

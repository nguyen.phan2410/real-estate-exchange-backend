package com.devcamp.realestateexchange.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "_name")
    private String name;

    @Column(name = "_province_id")
    private Integer provinceId;

    @Column(name = "_district_id")
    private Integer districtId;

    @Column(name = "_ward_id")
    private Integer wardId;

    private String address;

    @Column(name = "_street_id")
    private Integer streetId;

    private String slogan;

    private String description;

    private BigDecimal acreage;

    @Column(name = "construct_area")
    private BigDecimal constructArea;

    @Column(name = "num_block")
    private Integer numBlock;

    @Column(name = "num_floors")
    private String numFloors;

    @NotNull(message = "Number of apartment must not be null")
    @Column(name = "num_apartment")
    private Integer numApartment;

    @Column(name = "apartmentt_area")
    private String apartmentArea;

    @Column(name = "investor")
    @NotNull(message = "Investor must not be null")
    private Integer investor;

    @Column(name = "construction_contractor")
    private Integer constructionContractor;

    @Column(name = "design_unit")
    private Integer designUnit;

    @NotEmpty(message = "Utilities must not be empty")
    private String utilities;

    @NotEmpty(message = "Region link must not be empty")
    @Column(name = "region_link")
    private String regionLink;

    private String photo;

    @Column(name = "_lat")
    private Double latitude;

    @Column(name = "_lng")
    private Double longtitude;

    public Project() {
    }

    public Project(Integer id, String name, Integer provinceId, Integer districtId, Integer wardId, String address,
            Integer streetId, String slogan, String description, BigDecimal acreage,
            BigDecimal constructArea, Integer numBlock, String numFloors, Integer numApartment, String apartmentArea,
            Integer investor, Integer constructionContractor, Integer designUnit, String utilities, String regionLink,
            String photo, Double latitude, Double longtitude) {
        this.id = id;
        this.name = name;
        this.provinceId = provinceId;
        this.districtId = districtId;
        this.wardId = wardId;
        this.address = address;
        this.streetId = streetId;
        this.slogan = slogan;
        this.description = description;
        this.acreage = acreage;
        this.constructArea = constructArea;
        this.numBlock = numBlock;
        this.numFloors = numFloors;
        this.numApartment = numApartment;
        this.apartmentArea = apartmentArea;
        this.investor = investor;
        this.constructionContractor = constructionContractor;
        this.designUnit = designUnit;
        this.utilities = utilities;
        this.regionLink = regionLink;
        this.photo = photo;
        this.latitude = latitude;
        this.longtitude = longtitude;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProvinceId() {
        return this.provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return this.districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getWardId() {
        return this.wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    public Integer getStreetId() {
        return this.streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public String getSlogan() {
        return this.slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAcreage() {
        return this.acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public BigDecimal getConstructArea() {
        return this.constructArea;
    }

    public void setConstructArea(BigDecimal constructArea) {
        this.constructArea = constructArea;
    }

    public Integer getNumBlock() {
        return this.numBlock;
    }

    public void setNumBlock(Integer numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return this.numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public Integer getNumApartment() {
        return this.numApartment;
    }

    public void setNumApartment(Integer numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmentArea() {
        return this.apartmentArea;
    }

    public void setApartmentArea(String apartmentArea) {
        this.apartmentArea = apartmentArea;
    }

    public Integer getInvestor() {
        return this.investor;
    }

    public void setInvestor(Integer investor) {
        this.investor = investor;
    }

    public Integer getConstructionContractor() {
        return this.constructionContractor;
    }

    public void setConstructionContractor(Integer constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public Integer getDesignUnit() {
        return this.designUnit;
    }

    public void setDesignUnit(Integer designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return this.utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return this.regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongtitude() {
        return this.longtitude;
    }

    public void setLongtitude(Double longitude) {
        this.longtitude = longitude;
    }

}

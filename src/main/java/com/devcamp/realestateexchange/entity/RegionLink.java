package com.devcamp.realestateexchange.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "region_link")
public class RegionLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Name must not be null")
    private String name;

    private String description;

    private String photo;

    private String address;

    @Column(name = "_lat")
    private Double lattitude;

    @Column(name = "_lng")
    private Double longtitude;

    public RegionLink() {
    }

    public RegionLink(Integer id, String name, String description, String photo, String address, Double lattitude,
            Double longtitude) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.address = address;
        this.lattitude = lattitude;
        this.longtitude = longtitude;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLattitude() {
        return this.lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongtitude() {
        return this.longtitude;
    }

    public void setLongtitude(Double longtitude) {
        this.longtitude = longtitude;
    }

}

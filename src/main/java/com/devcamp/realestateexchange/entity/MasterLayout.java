package com.devcamp.realestateexchange.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "master_layout")
public class MasterLayout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Name must not be empty")
    private String name;

    private String description;

    @Column(name = "project_id")
    @NotNull(message = "Project id must not be null")
    private Integer projectId;

    private BigDecimal acreage;

    @Column(name = "apartment_list")
    private String apartmentList;

    private String photo;

    @Column(name = "date_create")
    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Timestamp dateCreate;

    @Column(name = "date_update")
    @UpdateTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Timestamp dateUpdate;

    public MasterLayout() {
    }

    public MasterLayout(Integer id, String name, String description, Integer projectId, BigDecimal acreage,
            String apartmentList, String photo, Timestamp dateCreate, Timestamp dateUpdate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.acreage = acreage;
        this.apartmentList = apartmentList;
        this.photo = photo;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getProjectId() {
        return this.projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public BigDecimal getAcreage() {
        return this.acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public String getApartmentList() {
        return this.apartmentList;
    }

    public void setApartmentList(String apartmentList) {
        this.apartmentList = apartmentList;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Timestamp getDateCreate() {
        return this.dateCreate;
    }

    public void setDateCreate(Timestamp dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Timestamp getDateUpdate() {
        return this.dateUpdate;
    }

    public void setDateUpdate(Timestamp dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

}

package com.devcamp.realestateexchange.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "realestate")
public class Realestate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private Integer type;

    private Integer request;

    @Column(name = "province_id")
    private Integer provinceId;

    @Column(name = "district_id")
    private Integer districtId;

    @Column(name = "wards_id")
    private Integer wardsId;

    @Column(name = "street_id")
    private Integer streetId;

    @Column(name = "project_id")
    private Integer projectId;

    @NotEmpty(message = "Address must not be empty")
    private String address;

    @Column(name = "customer_id")
    private Integer customerId;

    private Long price;

    @Column(name = "price_min")
    private Long priceMin;

    @Column(name = "price_time")
    private Integer priceTime;

    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateCreate;

    private BigDecimal acreage;

    private Integer direction;

    @Column(name = "total_floors")
    private Integer totalFloors;

    @Column(name = "number_floors")
    private Integer numberFloors;

    private Integer bath;

    @Column(name = "apart_code")
    private String apartCode;

    @Column(name = "wall_area")
    private BigDecimal wallArea;

    private Integer bedroom;

    private Integer balcony;

    @Column(name = "landscape_view")
    private String landscapeView;

    @Column(name = "apart_loca")
    private Integer apartLoca;

    @Column(name = "apart_type")
    private Integer apartType;

    @Column(name = "furniture_type")
    private Integer furnitureType;

    @Column(name = "price_rent")
    private Integer priceRent;

    @Column(name = "return_rate")
    private Double returnRate;

    @Column(name = "legal_doc")
    private Integer legalDoc;

    private String description;

    @Column(name = "width_y")
    private Integer widthY;

    @Column(name = "long_x")
    private Integer longX;

    @Column(name = "street_house")
    private Integer streetHouse;

    @Column(name = "FSBO")
    private Integer fsbo;

    @Column(name = "view_num")
    private Integer viewNum;

    @Column(name = "create_by")
    private Integer createBy;

    @Column(name = "update_by")
    private Integer updateBy;

    private String shape;

    @Column(name = "distance2facade")
    private Integer distance2Facade;

    @Column(name = "adjacent_facade_num")
    private Integer adjacentFacadeNum;

    @Column(name = "adjacent_road")
    private String adjacentRoad;

    @Column(name = "alley_min_width")
    private Integer alleyMinWidth;

    @Column(name = "adjacent_alley_min_width")
    private Integer adjacentAlleyMinWidth;

    private Integer factor;

    private String structure;

    @Column(name = "DTSXD")
    private Integer dtsxd;

    @Column(name = "CLCL")
    private Integer clcl;

    @Column(name = "CTXD_price")
    private Integer ctxdPrice;

    @Column(name = "CTXD_value")
    private Integer ctxdValue;

    private String photo;

    @Column(name = "_lat")
    private Double latitude;

    @Column(name = "_lng")
    private Double longtitude;

    @Column(name = "Approved")
    private Integer approved;

    public Realestate() {
    }

    public Realestate(Integer id, String title, Integer type, Integer request, Integer provinceId, Integer districtId,
            Integer wardsId, Integer streetId, Integer projectId, String address, Integer customerId, Long price,
            Long priceMin, Integer priceTime, Date dateCreate, BigDecimal acreage, Integer direction,
            Integer totalFloors, Integer numberFloors, Integer bath, String apartCode, BigDecimal wallArea,
            Integer bedroom, Integer balcony, String landscapeView, Integer apartLoca, Integer apartType,
            Integer furnitureType, Integer priceRent, Double returnRate, Integer legalDoc, String description,
            Integer widthY, Integer longX, Integer streetHouse, Integer fsbo, Integer viewNum, Integer createBy,
            Integer updateBy, String shape, Integer distance2Facade, Integer adjacentFacadeNum, String adjacentRoad,
            Integer alleyMinWidth, Integer adjacentAlleyMinWidth, Integer factor, String structure, Integer dtsxd,
            Integer clcl, Integer ctxdPrice, Integer ctxdValue, String photo, Double latitude, Double longtitude,
            Integer approved) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.request = request;
        this.provinceId = provinceId;
        this.districtId = districtId;
        this.wardsId = wardsId;
        this.streetId = streetId;
        this.projectId = projectId;
        this.address = address;
        this.customerId = customerId;
        this.price = price;
        this.priceMin = priceMin;
        this.priceTime = priceTime;
        this.dateCreate = dateCreate;
        this.acreage = acreage;
        this.direction = direction;
        this.totalFloors = totalFloors;
        this.numberFloors = numberFloors;
        this.bath = bath;
        this.apartCode = apartCode;
        this.wallArea = wallArea;
        this.bedroom = bedroom;
        this.balcony = balcony;
        this.landscapeView = landscapeView;
        this.apartLoca = apartLoca;
        this.apartType = apartType;
        this.furnitureType = furnitureType;
        this.priceRent = priceRent;
        this.returnRate = returnRate;
        this.legalDoc = legalDoc;
        this.description = description;
        this.widthY = widthY;
        this.longX = longX;
        this.streetHouse = streetHouse;
        this.fsbo = fsbo;
        this.viewNum = viewNum;
        this.createBy = createBy;
        this.updateBy = updateBy;
        this.shape = shape;
        this.distance2Facade = distance2Facade;
        this.adjacentFacadeNum = adjacentFacadeNum;
        this.adjacentRoad = adjacentRoad;
        this.alleyMinWidth = alleyMinWidth;
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
        this.factor = factor;
        this.structure = structure;
        this.dtsxd = dtsxd;
        this.clcl = clcl;
        this.ctxdPrice = ctxdPrice;
        this.ctxdValue = ctxdValue;
        this.photo = photo;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.approved = approved;
    }

    public Integer getApproved() {
        return this.approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRequest() {
        return this.request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Integer getProvinceId() {
        return this.provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return this.districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getWardsId() {
        return this.wardsId;
    }

    public void setWardsId(Integer wardsId) {
        this.wardsId = wardsId;
    }

    public Integer getStreetId() {
        return this.streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public Integer getProjectId() {
        return this.projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCustomerId() {
        return this.customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Long getPrice() {
        return this.price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getPriceMin() {
        return this.priceMin;
    }

    public void setPriceMin(Long priceMin) {
        this.priceMin = priceMin;
    }

    public Integer getPriceTime() {
        return this.priceTime;
    }

    public void setPriceTime(Integer priceTime) {
        this.priceTime = priceTime;
    }

    public Date getDateCreate() {
        return this.dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public BigDecimal getAcreage() {
        return this.acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public Integer getDirection() {
        return this.direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getTotalFloors() {
        return this.totalFloors;
    }

    public void setTotalFloors(Integer totalFloors) {
        this.totalFloors = totalFloors;
    }

    public Integer getNumberFloors() {
        return this.numberFloors;
    }

    public void setNumberFloors(Integer numberFloors) {
        this.numberFloors = numberFloors;
    }

    public Integer getBath() {
        return this.bath;
    }

    public void setBath(Integer bath) {
        this.bath = bath;
    }

    public String getApartCode() {
        return this.apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public BigDecimal getWallArea() {
        return this.wallArea;
    }

    public void setWallArea(BigDecimal wallArea) {
        this.wallArea = wallArea;
    }

    public Integer getBedroom() {
        return this.bedroom;
    }

    public void setBedroom(Integer bedroom) {
        this.bedroom = bedroom;
    }

    public Integer getBalcony() {
        return this.balcony;
    }

    public void setBalcony(Integer balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return this.landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public Integer getApartLoca() {
        return this.apartLoca;
    }

    public void setApartLoca(Integer apartLoca) {
        this.apartLoca = apartLoca;
    }

    public Integer getApartType() {
        return this.apartType;
    }

    public void setApartType(Integer apartType) {
        this.apartType = apartType;
    }

    public Integer getFurnitureType() {
        return this.furnitureType;
    }

    public void setFurnitureType(Integer furnitureType) {
        this.furnitureType = furnitureType;
    }

    public Integer getPriceRent() {
        return this.priceRent;
    }

    public void setPriceRent(Integer priceRent) {
        this.priceRent = priceRent;
    }

    public Double getReturnRate() {
        return this.returnRate;
    }

    public void setReturnRate(Double returnRate) {
        this.returnRate = returnRate;
    }

    public Integer getLegalDoc() {
        return this.legalDoc;
    }

    public void setLegalDoc(Integer legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWidthY() {
        return this.widthY;
    }

    public void setWidthY(Integer widthY) {
        this.widthY = widthY;
    }

    public Integer getLongX() {
        return this.longX;
    }

    public void setLongX(Integer longX) {
        this.longX = longX;
    }

    public Integer getStreetHouse() {
        return this.streetHouse;
    }

    public void setStreetHouse(Integer streetHouse) {
        this.streetHouse = streetHouse;
    }

    public Integer getFsbo() {
        return this.fsbo;
    }

    public void setFsbo(Integer fsbo) {
        this.fsbo = fsbo;
    }

    public Integer getViewNum() {
        return this.viewNum;
    }

    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public Integer getCreateBy() {
        return this.createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Integer getUpdateBy() {
        return this.updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getShape() {
        return this.shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Integer getDistance2Facade() {
        return this.distance2Facade;
    }

    public void setDistance2Facade(Integer distance2Facade) {
        this.distance2Facade = distance2Facade;
    }

    public Integer getAdjacentFacadeNum() {
        return this.adjacentFacadeNum;
    }

    public void setAdjacentFacadeNum(Integer adjacentFacadeNum) {
        this.adjacentFacadeNum = adjacentFacadeNum;
    }

    public String getAdjacentRoad() {
        return this.adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public Integer getAlleyMinWidth() {
        return this.alleyMinWidth;
    }

    public void setAlleyMinWidth(Integer alleyMinWidth) {
        this.alleyMinWidth = alleyMinWidth;
    }

    public Integer getAdjacentAlleyMinWidth() {
        return this.adjacentAlleyMinWidth;
    }

    public void setAdjacentAlleyMinWidth(Integer adjacentAlleyMinWidth) {
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
    }

    public Integer getFactor() {
        return this.factor;
    }

    public void setFactor(Integer factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return this.structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Integer getDtsxd() {
        return this.dtsxd;
    }

    public void setDtsxd(Integer dtsxd) {
        this.dtsxd = dtsxd;
    }

    public Integer getClcl() {
        return this.clcl;
    }

    public void setClcl(Integer clcl) {
        this.clcl = clcl;
    }

    public Integer getCtxdPrice() {
        return this.ctxdPrice;
    }

    public void setCtxdPrice(Integer ctxdPrice) {
        this.ctxdPrice = ctxdPrice;
    }

    public Integer getCtxdValue() {
        return this.ctxdValue;
    }

    public void setCtxdValue(Integer ctxdValue) {
        this.ctxdValue = ctxdValue;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongtitude() {
        return this.longtitude;
    }

    public void setLongtitude(Double longtitude) {
        this.longtitude = longtitude;
    }

}

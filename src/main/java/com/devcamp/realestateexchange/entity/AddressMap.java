package com.devcamp.realestateexchange.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "address_map")
public class AddressMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotEmpty(message = "Address must not be empty")
    private String address;

    @Column(name = "_lat")
    @NotNull(message = "Latitude must not be null")
    private Double lattitude;

    @Column(name = "_lng")
    @NotNull(message = "Longitude must not be null")
    private Double longtitude;

    public AddressMap() {
    }

    public AddressMap(Integer id, String address, Double lattitude, Double longtitude) {
        this.id = id;
        this.address = address;
        this.lattitude = lattitude;
        this.longtitude = longtitude;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLattitude() {
        return this.lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongtitude() {
        return this.longtitude;
    }

    public void setLongtitude(Double longtitude) {
        this.longtitude = longtitude;
    }

}

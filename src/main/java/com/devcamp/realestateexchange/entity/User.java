package com.devcamp.realestateexchange.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class User {

    @Column(name = "Username")
    private String username;

    @Column(name = "Password")
    private String password;

    @Column(name = "Email", unique = true)
    private String email;

    @ManyToOne
    @JoinColumn(name = "UserLevel")
    private Role userLevel;

    public User() {
    }

    public User(String username, String email, String password, Role userLevel) {
        this.username = username;
        this.password = password;
        this.userLevel = userLevel;
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getUserLevel() {
        return this.userLevel;
    }

    public void setUserLevel(Role userLevel) {
        this.userLevel = userLevel;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
package com.devcamp.realestateexchange.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "subscriptions")
public class Subscription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String user;

    @NotEmpty(message = "Endpoint must not be null")
    private String endpoint;

    @NotEmpty(message = "Public key must not be null")
    private String publickey;

    @NotEmpty(message = "Authentication token must not be empty")
    private String authenticationtoken;

    @NotEmpty(message = "Content encoding must not be empty")
    private String contentencoding;

    public Subscription() {
    }

    public Subscription(Integer id, String user, String endpoint, String publickey, String authenticationtoken,
            String contentencoding) {
        this.id = id;
        this.user = user;
        this.endpoint = endpoint;
        this.publickey = publickey;
        this.authenticationtoken = authenticationtoken;
        this.contentencoding = contentencoding;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEndpoint() {
        return this.endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getPublickey() {
        return this.publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    public String getAuthenticationtoken() {
        return this.authenticationtoken;
    }

    public void setAuthenticationtoken(String authenticationtoken) {
        this.authenticationtoken = authenticationtoken;
    }

    public String getContentencoding() {
        return this.contentencoding;
    }

    public void setContentencoding(String contentencoding) {
        this.contentencoding = contentencoding;
    }

}

package com.devcamp.realestateexchange.security.services;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.realestateexchange.entity.Customer;
import com.devcamp.realestateexchange.entity.Employee;
import com.devcamp.realestateexchange.entity.User;
import com.devcamp.realestateexchange.repository.CustomerRepository;
import com.devcamp.realestateexchange.repository.EmployeeRepository;
import com.devcamp.realestateexchange.security.jwt.JwtUtils;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  @Autowired
  CustomerRepository customerRepository;

  @Autowired
  EmployeeRepository employeeRepository;

  @Autowired
  private JwtUtils jwtUtils;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    String[] parts = username.split("_", 2);
    username = parts[1];
    String userType = parts[0];
    if (userType.equals("Customer")) {
      Customer customer = customerRepository.findByUsername(username);
      if (customer != null) {
        return UserDetailsImpl.build(customer);
      } else {
        throw new UsernameNotFoundException("User Not Found with username: " + username);
      }
    }

    Employee employee = employeeRepository.findByUsername(username);
    if (employee != null) {
      return UserDetailsImpl.build(employee);
    } else {
      throw new UsernameNotFoundException("User Not Found with username: " + username);
    }

  }

  public User whoami(HttpServletRequest req) {
    String token = resolveToken(req);

    String userType_username = jwtUtils.getUserNameFromJwtToken(token);
    String userType = userType_username.split("_", 2)[0];
    String username = userType_username.split("_", 2)[1];
    if (userType.equals("Customer")) {
      return customerRepository.findByUsername(username);
    }

    return employeeRepository.findByUsername(username);
  }

  private String resolveToken(HttpServletRequest req) {
    String bearerToken = req.getHeader("Authorization");
    if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
      return bearerToken.substring(7);
    }
    return null;
  }
}

package com.devcamp.realestateexchange.model;

public enum ERole {
  ROLE_ANONYMOUS,
  ROLE_ADMINISTRATOR,
  ROLE_DEFAULT,
  ROLE_HOMESELLER,
}

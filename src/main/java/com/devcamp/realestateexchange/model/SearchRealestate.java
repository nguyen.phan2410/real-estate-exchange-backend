package com.devcamp.realestateexchange.model;

public class SearchRealestate {
    private Integer provinceId;
    private Integer districtId;
    private Integer wardsId;
    private Integer streetId;
    private Integer type;
    private Integer request;
    private Integer bedroom;
    private Long priceLow;
    private Long priceHigh;
    private String title;

    public Integer getProvinceId() {
        return this.provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return this.districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getWardsId() {
        return this.wardsId;
    }

    public void setWardsId(Integer wardsId) {
        this.wardsId = wardsId;
    }

    public Integer getStreetId() {
        return this.streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRequest() {
        return this.request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Integer getBedroom() {
        return this.bedroom;
    }

    public void setBedroom(Integer bedroom) {
        this.bedroom = bedroom;
    }

    public Long getPriceLow() {
        return this.priceLow;
    }

    public void setPriceLow(Long priceLow) {
        this.priceLow = priceLow;
    }

    public Long getPriceHigh() {
        return this.priceHigh;
    }

    public void setPriceHigh(Long priceHigh) {
        this.priceHigh = priceHigh;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestateexchange.entity.MasterLayout;
import com.devcamp.realestateexchange.service.FileStorageService;
import com.devcamp.realestateexchange.service.MasterLayoutService;

@CrossOrigin
@RestController
public class MasterLayoutController {
    @Autowired
    MasterLayoutService masterLayoutService;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping("/masterLayouts")
    public ResponseEntity<List<MasterLayout>> getAllMasterLayouts(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "0") int size) {
        try {
            return new ResponseEntity<>(masterLayoutService.getAllMasterLayouts(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/masterLayouts/dataTable")
    public ResponseEntity<Object> getAllMasterLayoutsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(masterLayoutService.getAllMasterLayoutsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/masterLayouts/{masterLayoutId}")
    public ResponseEntity<MasterLayout> getMasterLayoutById(@PathVariable int masterLayoutId) {
        try {
            MasterLayout masterLayout = masterLayoutService.getMasterLayoutById(masterLayoutId);
            if (masterLayout == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(masterLayout, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/masterLayouts")
    public ResponseEntity<Object> createMasterLayout(@Valid @ModelAttribute MasterLayout masterLayout,
            @RequestParam("image") MultipartFile file) {
        try {
            // load file to system
            String fileName = fileStorageService.saveFile(file);
            if (fileName != null) {
                masterLayout.setPhoto(fileName);
            }
            return new ResponseEntity<>(masterLayoutService.createMasterLayout(masterLayout), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/masterLayouts/{masterLayoutId}")
    public ResponseEntity<Object> updateMasterLayout(@PathVariable int masterLayoutId,
            @Valid @ModelAttribute MasterLayout masterLayout, @RequestParam("image") MultipartFile file) {
        try {
            if (!file.getOriginalFilename().equals(masterLayout.getPhoto())) {
                // load file to system
                String fileName = fileStorageService.saveFile(file);
                // delete old file
                fileStorageService.deleteFile(masterLayout.getPhoto());
                masterLayout.setPhoto(fileName);
            }

            // update data
            MasterLayout updatedMasterLayout = masterLayoutService.updateMasterLayout(masterLayoutId, masterLayout);
            if (updatedMasterLayout == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedMasterLayout, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/masterLayouts/{masterLayoutId}")
    public ResponseEntity<Object> deleteMasterLayoutById(@PathVariable int masterLayoutId) {
        try {

            MasterLayout masterLayout = masterLayoutService.deleteMasterLayoutById(masterLayoutId);
            if (masterLayout == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                // delete file
                fileStorageService.deleteFile(masterLayout.getPhoto());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

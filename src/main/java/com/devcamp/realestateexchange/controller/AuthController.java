package com.devcamp.realestateexchange.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.Customer;
import com.devcamp.realestateexchange.entity.Employee;
import com.devcamp.realestateexchange.entity.User;
import com.devcamp.realestateexchange.model.ERole;
import com.devcamp.realestateexchange.payload.request.LoginRequest;
import com.devcamp.realestateexchange.payload.request.SignupRequest;
import com.devcamp.realestateexchange.payload.response.JwtResponse;
import com.devcamp.realestateexchange.payload.response.MessageResponse;
import com.devcamp.realestateexchange.repository.CustomerRepository;
import com.devcamp.realestateexchange.repository.EmployeeRepository;
import com.devcamp.realestateexchange.repository.RoleRepository;
import com.devcamp.realestateexchange.security.jwt.JwtUtils;
import com.devcamp.realestateexchange.security.services.UserDetailsImpl;
import com.devcamp.realestateexchange.security.services.UserDetailsServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  CustomerRepository customerRepository;

  @Autowired
  EmployeeRepository employeeRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  UserDetailsServiceImpl userDetailsServiceImpl;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  JwtUtils jwtUtils;

  @PostMapping("/signin/{userType}")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest,
      @PathVariable String userType) {

    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(userType + "_" + loginRequest.getUsername(),
            loginRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    userDetails.setUsername(userType + "_" + loginRequest.getUsername());
    String jwt = jwtUtils.generateJwtToken(authentication);

    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    return ResponseEntity.ok(new JwtResponse(jwt,
        userDetails.getUsername().split("_", 2)[1],
        userDetails.getEmail(),
        roles));
  }

  @GetMapping("/me")
  public User userAccess(HttpServletRequest req) {
    return userDetailsServiceImpl.whoami(req);
  }

  @PostMapping("/signup/{userType}")
  public ResponseEntity<?> registerUser(@PathVariable String userType,
      @Valid @RequestBody SignupRequest signUpRequest) {
    try {
      // --------------------If request sent from Customer Web --------------------
      if (userType.equals("Customer")) {
        if (customerRepository.existsByUsername(signUpRequest.getUsername())) {
          return ResponseEntity
              .badRequest()
              .body("Username is already taken!");
        }

        if (customerRepository.existsByEmail(signUpRequest.getEmail())) {
          return ResponseEntity
              .badRequest()
              .body("Email is already taken!");
        }

        // Create new user's account
        Customer customer = new Customer();
        customer.setUsername(signUpRequest.getUsername());
        customer.setEmail(signUpRequest.getEmail());
        customer.setPassword(encoder.encode(signUpRequest.getPassword()));
        customer.setUserLevel(roleRepository.findByName(ERole.ROLE_DEFAULT));
        customerRepository.save(customer);

        return ResponseEntity.ok("User registered successfully!");
      }

      // --------------------If request sent from Admin Dashboard --------------------
      if (employeeRepository.existsByUsername(signUpRequest.getUsername())) {
        return ResponseEntity
            .badRequest()
            .body("Username is already taken!");
      }

      if (employeeRepository.existsByEmail(signUpRequest.getEmail())) {
        return ResponseEntity
            .badRequest()
            .body("Email is already taken!");
      }
      // Create new user's account
      Employee employee = new Employee();
      employee.setUsername(signUpRequest.getUsername());
      employee.setEmail(signUpRequest.getEmail());
      employee.setPassword(encoder.encode(signUpRequest.getPassword()));
      employee.setUserLevel(roleRepository.findByName(ERole.ROLE_DEFAULT));
      employee.setFirstName(signUpRequest.getFirstName());
      employee.setLastName(signUpRequest.getLastName());
      employeeRepository.save(employee);
      return ResponseEntity.ok("User registered successfully!");
    } catch (Exception e) {
      // TODO: handle exception
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

    }

  }

}

package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.Street;
import com.devcamp.realestateexchange.service.StreetService;

@CrossOrigin
@RestController
public class StreetController {
    @Autowired
    StreetService streetService;

    @GetMapping("/streets")
    public ResponseEntity<List<Street>> getAllStreets(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            return new ResponseEntity<>(streetService.getAllStreets(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/streets/dataTable")
    public ResponseEntity<Object> getAllStreetsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(streetService.getAllStreetsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/streets/{streetId}")
    public ResponseEntity<Street> getStreetById(@PathVariable int streetId) {
        try {
            Street street = streetService.getStreetById(streetId);
            if (street == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(street, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{districtId}/streets")
    public ResponseEntity<List<Street>> getStreetsByDistrictId(@PathVariable int districtId) {
        try {
            List<Street> district = streetService.getStreetsByDistrictId(districtId);
            if (district == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(district, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/streets")
    public ResponseEntity<Street> createStreet(@Valid @RequestBody Street street) {
        try {
            return new ResponseEntity<>(streetService.createStreet(street), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/streets/{streetId}")
    public ResponseEntity<Street> updateStreet(@PathVariable int streetId,
            @Valid @RequestBody Street street) {
        try {
            Street updatedStreet = streetService.updateStreet(streetId, street);
            if (updatedStreet == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedStreet, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/streets/{streetId}")
    public ResponseEntity<Object> deleteStreetById(@PathVariable int streetId) {
        try {
            Street street = streetService.deleteStreetById(streetId);
            if (street == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

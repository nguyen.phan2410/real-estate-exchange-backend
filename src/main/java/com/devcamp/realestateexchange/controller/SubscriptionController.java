package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.Subscription;
import com.devcamp.realestateexchange.service.SubscriptionService;

@CrossOrigin
@RestController
public class SubscriptionController {
    @Autowired
    SubscriptionService subscriptionService;

    @GetMapping("/subscriptions")
    public ResponseEntity<List<Subscription>> getAllSubscriptions(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            return new ResponseEntity<>(subscriptionService.getAllSubscriptions(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/subscriptions/dataTable")
    public ResponseEntity<Object> getAllSubscriptionsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(subscriptionService.getAllSubscriptionsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/subscriptions/{subscriptionId}")
    public ResponseEntity<Subscription> getSubscriptionById(@PathVariable int subscriptionId) {
        try {
            Subscription subscription = subscriptionService.getSubscriptionById(subscriptionId);
            if (subscription == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(subscription, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/subscriptions")
    public ResponseEntity<Subscription> createSubscription(@Valid @RequestBody Subscription subscription) {
        try {
            return new ResponseEntity<>(subscriptionService.createSubscription(subscription), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/subscriptions/{subscriptionId}")
    public ResponseEntity<Subscription> updateSubscription(@PathVariable int subscriptionId,
            @Valid @RequestBody Subscription subscription) {
        try {
            Subscription updatedSubscription = subscriptionService.updateSubscription(subscriptionId, subscription);
            if (updatedSubscription == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedSubscription, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/subscriptions/{subscriptionId}")
    public ResponseEntity<Object> deleteSubscriptionById(@PathVariable int subscriptionId) {
        try {
            Subscription subscription = subscriptionService.deleteSubscriptionById(subscriptionId);
            if (subscription == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

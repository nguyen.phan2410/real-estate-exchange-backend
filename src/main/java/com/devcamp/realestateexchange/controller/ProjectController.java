package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestateexchange.entity.Project;
import com.devcamp.realestateexchange.service.FileStorageService;
import com.devcamp.realestateexchange.service.ProjectService;

@CrossOrigin
@RestController
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping("/projects")
    public ResponseEntity<List<Project>> getAllProjects(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "0") int size) {
        try {
            return new ResponseEntity<>(projectService.getAllProjects(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/projects/dataTable")
    public ResponseEntity<Object> getAllProjectsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(projectService.getAllProjectsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/projects/{projectId}")
    public ResponseEntity<Project> getProjectById(@PathVariable int projectId) {
        try {
            Project project = projectService.getProjectById(projectId);
            if (project == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(project, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{districtId}/projects")
    public ResponseEntity<List<Project>> getProjectsByDistrictId(@PathVariable int districtId) {
        try {
            List<Project> district = projectService.getProjectsByDistrictId(districtId);
            if (district == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(district, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/projects")
    public ResponseEntity<Object> createProject(@Valid @ModelAttribute Project project,
            @RequestParam("image") MultipartFile file) {
        try {
            // load file to system
            String fileName = fileStorageService.saveFile(file);
            if (fileName != null) {
                project.setPhoto(fileName);
            }
            return new ResponseEntity<>(projectService.createProject(project), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/projects/{projectId}")
    public ResponseEntity<Object> updateProject(@PathVariable int projectId,
            @Valid @ModelAttribute Project project, @RequestParam("image") MultipartFile file) {
        try {
            if (!file.getOriginalFilename().equals(project.getPhoto())) {
                // load file to system
                String fileName = fileStorageService.saveFile(file);
                // delete old file
                fileStorageService.deleteFile(project.getPhoto());
                project.setPhoto(fileName);
            }

            // update data
            Project updatedProject = projectService.updateProject(projectId, project);
            if (updatedProject == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedProject, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/projects/{projectId}")
    public ResponseEntity<Object> deleteProjectById(@PathVariable int projectId) {
        try {

            Project project = projectService.deleteProjectById(projectId);
            if (project == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                // delete file
                fileStorageService.deleteFile(project.getPhoto());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.ConstructionContractor;
import com.devcamp.realestateexchange.service.ConstructionContractorService;

@CrossOrigin
@RestController
public class ConstructionContractorController {
    @Autowired
    ConstructionContractorService constructionContractorService;

    @GetMapping("/constructionContractors")
    public ResponseEntity<List<ConstructionContractor>> getAllConstructionContractors(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            return new ResponseEntity<>(constructionContractorService.getAllConstructionContractors(page, size),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/constructionContractors/dataTable")
    public ResponseEntity<Object> getAllConstructionContractorsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(constructionContractorService.getAllConstructionContractorsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/constructionContractors/{constructionContractorId}")
    public ResponseEntity<ConstructionContractor> getConstructionContractorById(
            @PathVariable int constructionContractorId) {
        try {
            ConstructionContractor constructionContractor = constructionContractorService
                    .getConstructionContractorById(constructionContractorId);
            if (constructionContractor == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(constructionContractor, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/constructionContractors")
    public ResponseEntity<ConstructionContractor> createConstructionContractor(
            @Valid @RequestBody ConstructionContractor constructionContractor) {
        try {
            return new ResponseEntity<>(
                    constructionContractorService.createConstructionContractor(constructionContractor),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/constructionContractors/{constructionContractorId}")
    public ResponseEntity<ConstructionContractor> updateConstructionContractor(
            @PathVariable int constructionContractorId,
            @Valid @RequestBody ConstructionContractor constructionContractor) {
        try {
            ConstructionContractor updatedConstructionContractor = constructionContractorService
                    .updateConstructionContractor(constructionContractorId, constructionContractor);
            if (updatedConstructionContractor == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedConstructionContractor, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/constructionContractors/{constructionContractorId}")
    public ResponseEntity<Object> deleteConstructionContractorById(@PathVariable int constructionContractorId) {
        try {
            ConstructionContractor constructionContractor = constructionContractorService
                    .deleteConstructionContractorById(constructionContractorId);
            if (constructionContractor == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

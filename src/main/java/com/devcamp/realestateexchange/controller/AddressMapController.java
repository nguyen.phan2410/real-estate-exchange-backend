package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.AddressMap;
import com.devcamp.realestateexchange.service.AddressMapService;

@CrossOrigin
@RestController
// @PreAuthorize("hasRole('ADMINSTRATOR')")
public class AddressMapController {
    @Autowired
    AddressMapService addressMapService;

    @GetMapping("/addressMaps")
    public ResponseEntity<List<AddressMap>> getAllAddressMaps(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            return new ResponseEntity<>(addressMapService.getAllAddressMaps(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/addressMaps/dataTable")
    public ResponseEntity<Object> getAllAddressMapsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(addressMapService.getAllAddressMapsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/addressMaps/{addressMapId}")
    public ResponseEntity<AddressMap> getAddressMapById(@PathVariable int addressMapId) {
        try {
            AddressMap addressMap = addressMapService.getAddressMapById(addressMapId);
            if (addressMap == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(addressMap, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/addressMaps")
    public ResponseEntity<AddressMap> createAddressMap(@Valid @RequestBody AddressMap addressMap) {
        try {
            return new ResponseEntity<>(addressMapService.createAddressMap(addressMap), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/addressMaps/{addressMapId}")
    public ResponseEntity<AddressMap> updateAddressMap(@PathVariable int addressMapId,
            @Valid @RequestBody AddressMap addressMap) {
        try {
            AddressMap updatedAddressMap = addressMapService.updateAddressMap(addressMapId, addressMap);
            if (updatedAddressMap == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedAddressMap, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/addressMaps/{addressMapId}")
    public ResponseEntity<Object> deleteAddressMapById(@PathVariable int addressMapId) {
        try {
            AddressMap addressMap = addressMapService.deleteAddressMapById(addressMapId);
            if (addressMap == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

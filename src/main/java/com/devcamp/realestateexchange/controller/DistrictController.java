package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.District;
import com.devcamp.realestateexchange.service.DistrictService;

@CrossOrigin
@RestController
public class DistrictController {
    @Autowired
    DistrictService districtService;

    @GetMapping("/districts")
    public ResponseEntity<List<District>> getAllDistricts(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "0") int size) {
        try {
            return new ResponseEntity<>(districtService.getAllDistricts(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/dataTable")
    public ResponseEntity<Object> getAllDistrictsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(districtService.getAllDistrictsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/districts/{districtId}")
    public ResponseEntity<District> getDistrictById(@PathVariable int districtId) {
        try {
            District district = districtService.getDistrictById(districtId);
            if (district == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(district, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{provinceId}/districts")
    public ResponseEntity<List<District>> getDistrictsByProvinceId(@PathVariable int provinceId) {
        try {
            List<District> district = districtService.getDistrictsByProvinceId(provinceId);
            if (district == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(district, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/districts")
    public ResponseEntity<District> createDistrict(@Valid @RequestBody District district) {
        try {
            return new ResponseEntity<>(districtService.createDistrict(district), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/districts/{districtId}")
    public ResponseEntity<District> updateDistrict(@PathVariable int districtId,
            @Valid @RequestBody District district) {
        try {
            District updatedDistrict = districtService.updateDistrict(districtId, district);
            if (updatedDistrict == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedDistrict, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/districts/{districtId}")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable int districtId) {
        try {
            District district = districtService.deleteDistrictById(districtId);
            if (district == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

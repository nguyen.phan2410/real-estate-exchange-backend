package com.devcamp.realestateexchange.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestateexchange.entity.Realestate;
import com.devcamp.realestateexchange.model.SearchRealestate;
import com.devcamp.realestateexchange.service.FileStorageService;
import com.devcamp.realestateexchange.service.RealestateService;

@CrossOrigin
@RestController
public class RealestateController {
    @Autowired
    RealestateService realestateService;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping("/realestates")
    public ResponseEntity<List<Realestate>> getAllRealestates(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            return new ResponseEntity<>(realestateService.getAllRealestates(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/realestates/dataTable/search")
    public ResponseEntity<Object> getAllRealestatesDataTableBySearch(
            HttpServletRequest request, Realestate realestate) {
        try {
            return new ResponseEntity<>(realestateService.getAllRealestatesDataTableBySearch(request,
                    realestate),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/realestates/search")
    public ResponseEntity<List<Realestate>> getRealesateBySearch(@RequestBody SearchRealestate search,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {

            return new ResponseEntity<>(realestateService.getRealesateBySearch(search,
                    page, size), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/realestates/search/count")
    public ResponseEntity<Integer> countRealesateBySearch(@RequestBody SearchRealestate search) {
        try {

            return new ResponseEntity<>(realestateService.countRealesateBySearch(search),
                    HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers/{customerId}/realestates")
    public ResponseEntity<List<Realestate>> getAllRealestatesByCustomer(
            @PathVariable Integer customerId) {
        try {
            return new ResponseEntity<>(realestateService.getAllRealestatesByCustomer(customerId),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/realestates/{realestateId}")
    public ResponseEntity<Realestate> getRealestateById(@PathVariable int realestateId) {
        try {
            Realestate realestate = realestateService.getRealestateById(realestateId);
            if (realestate == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(realestate, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/realestates")
    public ResponseEntity<Object> createRealestate(@Valid @ModelAttribute Realestate realestate,
            @RequestParam("image") MultipartFile file) {
        try {
            // load file to system
            String fileName = fileStorageService.saveFile(file);
            if (fileName != null) {
                realestate.setPhoto(fileName);
            }
            return new ResponseEntity<>(realestateService.createRealestate(realestate), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/realestates/{realestateId}")
    public ResponseEntity<Object> updateRealestate(@PathVariable int realestateId,
            @Valid @ModelAttribute Realestate realestate, @RequestParam("image") MultipartFile file) {
        try {
            if (!file.getOriginalFilename().equals(realestate.getPhoto())) {
                // load file to system
                String fileName = fileStorageService.saveFile(file);
                // delete old file
                fileStorageService.deleteFile(realestate.getPhoto());
                realestate.setPhoto(fileName);
            }

            // update data
            Realestate updatedRealestate = realestateService.updateRealestate(realestateId, realestate);
            if (updatedRealestate == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedRealestate, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/realestates/{realestateId}")
    public ResponseEntity<Object> deleteRealestateById(@PathVariable int realestateId) {
        try {

            Realestate realestate = realestateService.deleteRealestateById(realestateId);
            if (realestate == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                // delete file
                fileStorageService.deleteFile(realestate.getPhoto());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

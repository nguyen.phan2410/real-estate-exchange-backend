package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.DesignUnit;
import com.devcamp.realestateexchange.service.DesignUnitService;

@CrossOrigin
@RestController
public class DesignUnitController {
    @Autowired
    DesignUnitService designUnitService;

    @GetMapping("/designUnits")
    public ResponseEntity<List<DesignUnit>> getAllDesignUnits(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            return new ResponseEntity<>(designUnitService.getAllDesignUnits(page, size),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/designUnits/dataTable")
    public ResponseEntity<Object> getAllDesignUnitsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(designUnitService.getAllDesignUnitsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/designUnits/{designUnitId}")
    public ResponseEntity<DesignUnit> getDesignUnitById(
            @PathVariable int designUnitId) {
        try {
            DesignUnit designUnit = designUnitService
                    .getDesignUnitById(designUnitId);
            if (designUnit == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(designUnit, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/designUnits")
    public ResponseEntity<DesignUnit> createDesignUnit(
            @Valid @RequestBody DesignUnit designUnit) {
        try {
            return new ResponseEntity<>(
                    designUnitService.createDesignUnit(designUnit),
                    HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/designUnits/{designUnitId}")
    public ResponseEntity<DesignUnit> updateDesignUnit(
            @PathVariable int designUnitId,
            @Valid @RequestBody DesignUnit designUnit) {
        try {
            DesignUnit updatedDesignUnit = designUnitService
                    .updateDesignUnit(designUnitId, designUnit);
            if (updatedDesignUnit == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedDesignUnit, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/designUnits/{designUnitId}")
    public ResponseEntity<Object> deleteDesignUnitById(@PathVariable int designUnitId) {
        try {
            DesignUnit designUnit = designUnitService
                    .deleteDesignUnitById(designUnitId);
            if (designUnit == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

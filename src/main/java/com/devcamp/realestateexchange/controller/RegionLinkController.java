package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestateexchange.entity.RegionLink;
import com.devcamp.realestateexchange.service.FileStorageService;
import com.devcamp.realestateexchange.service.RegionLinkService;

@CrossOrigin
@RestController
public class RegionLinkController {
    @Autowired
    RegionLinkService regionLinkService;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping("/regionLinks")
    public ResponseEntity<List<RegionLink>> getAllRegionLinks(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "0") int size) {
        try {
            return new ResponseEntity<>(regionLinkService.getAllRegionLinks(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/regionLinks/dataTable")
    public ResponseEntity<Object> getAllRegionLinksDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(regionLinkService.getAllRegionLinksDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/regionLinks/{regionLinkId}")
    public ResponseEntity<RegionLink> getRegionLinkById(@PathVariable int regionLinkId) {
        try {
            RegionLink regionLink = regionLinkService.getRegionLinkById(regionLinkId);
            if (regionLink == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(regionLink, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/regionLinks")
    public ResponseEntity<Object> createRegionLink(@Valid @ModelAttribute RegionLink regionLink,
            @RequestParam("image") MultipartFile file) {
        try {
            // load file to system
            String fileName = fileStorageService.saveFile(file);
            if (fileName != null) {
                regionLink.setPhoto(fileName);
            }
            return new ResponseEntity<>(regionLinkService.createRegionLink(regionLink), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/regionLinks/{regionLinkId}")
    public ResponseEntity<Object> updateRegionLink(@PathVariable int regionLinkId,
            @Valid @ModelAttribute RegionLink regionLink, @RequestParam("image") MultipartFile file) {
        try {
            if (!file.getOriginalFilename().equals(regionLink.getPhoto())) {
                System.out.println("HEYYYYYYYYYYYYYYYYYYYYY:" + "got ");
                // load file to system
                String fileName = fileStorageService.saveFile(file);
                // delete old file
                fileStorageService.deleteFile(regionLink.getPhoto());
                regionLink.setPhoto(fileName);
            }

            // update data
            RegionLink updatedRegionLink = regionLinkService.updateRegionLink(regionLinkId, regionLink);
            if (updatedRegionLink == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedRegionLink, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/regionLinks/{regionLinkId}")
    public ResponseEntity<Object> deleteRegionLinkById(@PathVariable int regionLinkId) {
        try {

            RegionLink regionLink = regionLinkService.deleteRegionLinkById(regionLinkId);
            if (regionLink == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                // delete file
                fileStorageService.deleteFile(regionLink.getPhoto());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

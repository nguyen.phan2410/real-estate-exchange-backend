package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.realestateexchange.entity.Utility;
import com.devcamp.realestateexchange.service.FileStorageService;
import com.devcamp.realestateexchange.service.UtilityService;

@CrossOrigin
@RestController
public class UtilityController {
    @Autowired
    UtilityService utilityService;

    @Autowired
    FileStorageService fileStorageService;

    @GetMapping("/utilitys")
    public ResponseEntity<List<Utility>> getAllUtilitys(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "0") int size) {
        try {
            return new ResponseEntity<>(utilityService.getAllUtilitys(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/utilitys/dataTable")
    public ResponseEntity<Object> getAllUtilitysDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(utilityService.getAllUtilitysDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/utilitys/{utilityId}")
    public ResponseEntity<Utility> getUtilityById(@PathVariable int utilityId) {
        try {
            Utility utility = utilityService.getUtilityById(utilityId);
            if (utility == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(utility, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/utilitys")
    public ResponseEntity<Object> createUtility(@Valid @ModelAttribute Utility utility,
            @RequestParam("image") MultipartFile file) {
        try {
            // load file to system
            String fileName = fileStorageService.saveFile(file);
            if (fileName != null) {
                utility.setPhoto(fileName);
            }
            return new ResponseEntity<>(utilityService.createUtility(utility), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/utilitys/{utilityId}")
    public ResponseEntity<Object> updateUtility(@PathVariable int utilityId,
            @Valid @ModelAttribute Utility utility, @RequestParam("image") MultipartFile file) {
        try {
            if (!file.getOriginalFilename().equals(utility.getPhoto())) {
                // load file to system
                String fileName = fileStorageService.saveFile(file);
                // delete old file
                fileStorageService.deleteFile(utility.getPhoto());
                utility.setPhoto(fileName);
            }

            // update data
            Utility updatedUtility = utilityService.updateUtility(utilityId, utility);
            if (updatedUtility == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedUtility, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/utilitys/{utilityId}")
    public ResponseEntity<Object> deleteUtilityById(@PathVariable int utilityId) {
        try {

            Utility utility = utilityService.deleteUtilityById(utilityId);
            if (utility == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                // delete file
                fileStorageService.deleteFile(utility.getPhoto());
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

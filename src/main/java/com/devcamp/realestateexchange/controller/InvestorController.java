package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.Investor;
import com.devcamp.realestateexchange.service.InvestorService;

@CrossOrigin
@RestController
public class InvestorController {
    @Autowired
    InvestorService investorService;

    @GetMapping("/investors")
    public ResponseEntity<List<Investor>> getAllInvestors(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size) {
        try {
            return new ResponseEntity<>(investorService.getAllInvestors(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investors/dataTable")
    public ResponseEntity<Object> getAllInvestorsDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(investorService.getAllInvestorsDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/investors/{investorId}")
    public ResponseEntity<Investor> getInvestorById(@PathVariable int investorId) {
        try {
            Investor investor = investorService.getInvestorById(investorId);
            if (investor == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(investor, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/investors")
    public ResponseEntity<Investor> createInvestor(@Valid @RequestBody Investor investor) {
        try {
            return new ResponseEntity<>(investorService.createInvestor(investor), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/investors/{investorId}")
    public ResponseEntity<Investor> updateInvestor(@PathVariable int investorId,
            @Valid @RequestBody Investor investor) {
        try {
            Investor updatedInvestor = investorService.updateInvestor(investorId, investor);
            if (updatedInvestor == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedInvestor, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/investors/{investorId}")
    public ResponseEntity<Object> deleteInvestorById(@PathVariable int investorId) {
        try {
            Investor investor = investorService.deleteInvestorById(investorId);
            if (investor == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

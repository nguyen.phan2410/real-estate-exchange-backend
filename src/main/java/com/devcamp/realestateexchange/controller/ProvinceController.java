package com.devcamp.realestateexchange.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchange.entity.Province;
import com.devcamp.realestateexchange.service.ProvinceService;

@CrossOrigin
@RestController
public class ProvinceController {
    @Autowired
    ProvinceService provinceService;

    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getAllProvinces(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "0") int size) {
        try {
            return new ResponseEntity<>(provinceService.getAllProvinces(page, size), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/dataTable")
    public ResponseEntity<Object> getAllProvincesDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(provinceService.getAllProvincesDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/provinces/{provinceId}")
    public ResponseEntity<Province> getProvinceById(@PathVariable int provinceId) {
        try {
            Province province = provinceService.getProvinceById(provinceId);
            if (province == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(province, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/provinces")
    public ResponseEntity<Province> createProvince(@Valid @RequestBody Province province) {
        try {
            return new ResponseEntity<>(provinceService.createProvince(province), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/provinces/{provinceId}")
    public ResponseEntity<Province> updateProvince(@PathVariable int provinceId,
            @Valid @RequestBody Province province) {
        try {
            Province updatedProvince = provinceService.updateProvince(provinceId, province);
            if (updatedProvince == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(updatedProvince, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/provinces/{provinceId}")
    public ResponseEntity<Object> deleteProvinceById(@PathVariable int provinceId) {
        try {
            Province province = provinceService.deleteProvinceById(provinceId);
            if (province == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

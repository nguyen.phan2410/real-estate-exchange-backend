# Real Estate Backend Project

This project is a backend web application for managing real estate listings. It provides a RESTful API for managing property listings, user accounts, and authentication.

## Technologies
Spring Boot, MySql

## API Endpoints

- `GET /{table}`: Get all the data in that table
- `GET /{table}/dataTable/search`: Get all the data in a table using dataTable
- `GET /{table}/{id}`: Get data in a table by id
- `POST /{table}`: Create new data in a table
- `PUT /{table}/{id}`: Update data in a table by id
- `DELETE /{table}/{id}`: Delete data in a table by id

- `POST /signup/{userType}`: Create a new user by user type (Customer/Employee)
- `POST /signin/{userType}`: Sign in as user by user type (Customer/Employee)
- `POST /me`: Check Token

